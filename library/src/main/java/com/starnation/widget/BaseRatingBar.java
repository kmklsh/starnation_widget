package com.starnation.widget;

import android.content.Context;
import android.support.v7.widget.AppCompatRatingBar;
import android.util.AttributeSet;
import android.view.MotionEvent;

/*
 * @author lsh
 * @since 2015. 12. 5.
*/
public class BaseRatingBar extends AppCompatRatingBar {

    //======================================================================
    // Variables
    //======================================================================

    private boolean mIsDragging;

    private OnDraggingTouchListener mDraggingTouchListener;

    //======================================================================
    // Constructor
    //======================================================================

    public BaseRatingBar(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
    }

    public BaseRatingBar(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    //======================================================================
    // Override Methods
    //======================================================================

    @Override
    public boolean onTouchEvent(MotionEvent event) {
        final boolean onTouch = super.onTouchEvent(event);

        if (onTouch == false) {
            return false;
        }

        switch(event.getAction()) {
            case MotionEvent.ACTION_MOVE:
                if (mIsDragging == false) {
                    onSupportStartTrackingTouch();
                }
                break;
            case MotionEvent.ACTION_UP:
            case MotionEvent.ACTION_CANCEL:
                if (mIsDragging == true) {
                    onSupportStopTrackingTouch();
                }
                break;
        }
        return true;
    }

    //======================================================================
    // Public Methods
    //======================================================================

    /**
     * @param draggingTouchListener {@link OnDraggingTouchListener}
     */
    public void setOnDraggingTouchListener(OnDraggingTouchListener draggingTouchListener) {
        mDraggingTouchListener = draggingTouchListener;
    }

    //======================================================================
    // Private Methods
    //======================================================================

    private void onSupportStartTrackingTouch() {
        mIsDragging = true;
        dispatchDraggingTouch();
    }

    private void onSupportStopTrackingTouch() {
        mIsDragging = false;
        dispatchDraggingTouch();
    }

    private void dispatchDraggingTouch() {
        if (mDraggingTouchListener != null) {
            mDraggingTouchListener.onDraggingTouch(mIsDragging);
        }
    }

    //======================================================================
    // OnTrackingTouchListener
    //======================================================================

    /**
     * {@link #onTouchEvent(MotionEvent)} 에서 호출 받은 인자값 {@link MotionEvent}값을 가지고
     * Dragging 동작을 구분
     */
    public interface OnDraggingTouchListener {

        /**
         * @param draggingStart Dragging 동작을 구분 Dragging 시작이면 true 종료면 false
         */
        void onDraggingTouch(boolean draggingStart);
    }
}
