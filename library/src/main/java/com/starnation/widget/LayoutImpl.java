package com.starnation.widget;

/*
 * @author lsh
 * @since 2015. 11. 26.
*/
interface LayoutImpl {
    void setChildInsets(Object insets, boolean drawStatusBar);
}
