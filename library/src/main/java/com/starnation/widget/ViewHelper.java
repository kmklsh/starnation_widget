package com.starnation.widget;

import android.content.Context;
import android.content.res.TypedArray;
import android.os.Parcel;
import android.os.Parcelable;
import android.support.annotation.NonNull;
import android.support.v4.content.ContextCompat;
import android.util.AttributeSet;
import android.view.View;
import android.widget.Checkable;


/*
 * @author lsh
 * @since 14. 12. 12.
*/
final class ViewHelper implements Checkable {

    //======================================================================
    // Constants
    //======================================================================

    public static final int RATIO_VERTICAL = 1;

    public static final int RATIO_HORIZONTAL = 2;

    //======================================================================
    // Variables
    //======================================================================

    private double mRatio;

    private int mNormalTextColor;

    private int mCheckedTextColor;

    private boolean mChecked;

    private int mRatioType = RATIO_VERTICAL;

    //======================================================================
    // Constructor
    //======================================================================

    public ViewHelper() {
    }

    //======================================================================
    // Override Methods
    //======================================================================

    @Override
    public void setChecked(boolean checked) {
        mChecked = checked;
    }

    @Override
    public boolean isChecked() {
        return mChecked;
    }

    @Override
    public void toggle() {
        setChecked(!mChecked);
    }

    //======================================================================
    // Public Methods
    //======================================================================

    public static void init(ViewHelper viewHelper, Context context, AttributeSet attrs) {
        if (attrs != null) {
            final TypedArray typedArray = context.obtainStyledAttributes(attrs, R.styleable.ViewHelper);

            viewHelper.setRatio(typedArray.getFloat(R.styleable.ViewHelper_ratio, 0));

            int checkedColor = typedArray.getResourceId(R.styleable.ViewHelper_checkedTextColor, 0);
            if (checkedColor != 0) {
                viewHelper.setCheckedTextColor(ContextCompat.getColor(context, checkedColor));
            }

            int normalColor = typedArray.getResourceId(R.styleable.ViewHelper_normalTextColor, 0);
            if (normalColor != 0) {
                viewHelper.setNormalTextColor(ContextCompat.getColor(context, normalColor));
            }

            int ratioType = typedArray.getInt(R.styleable.ViewHelper_ratioType, RATIO_VERTICAL);
            viewHelper.setRatioType(ratioType);

            typedArray.recycle();
        }
    }

    public Parcelable onSaveInstanceState(Parcelable parcelable) {
        SavedStateViewHelper state = new SavedStateViewHelper(parcelable);
        state.checkedTextColor = mCheckedTextColor;
        state.normalTextColor = mNormalTextColor;
        state.mRatio = mRatio;
        state.checked = mChecked;
        return state;
    }

    public void onRestoreInstanceState(SavedStateViewHelper state) {
        if (state != null) {
            mCheckedTextColor = state.checkedTextColor;
            mNormalTextColor = state.normalTextColor;
            mRatio = state.mRatio;
            mChecked = state.checked;
        }
    }

    public int buildMeasureSpecHeight(int widthMeasureSpec) {
        final int cx = View.MeasureSpec.getSize(widthMeasureSpec);
        final int cy = (int) Math.round(cx / mRatio);
        return View.MeasureSpec.makeMeasureSpec(cy, View.MeasureSpec.EXACTLY);
    }

    public int buildMeasureSpecWidth(int heightMeasureSpec) {
        final int cy = View.MeasureSpec.getSize(heightMeasureSpec);
        final int cx = (int) Math.round(cy * mRatio);
        return View.MeasureSpec.makeMeasureSpec(cx, View.MeasureSpec.EXACTLY);
    }

    public double getRatio() {
        return mRatio;
    }

    public boolean setRatio(double ratio) {
        if (mRatio != ratio) {
            mRatio = ratio;
            return true;
        }
        return false;
    }

    public boolean isCheckedTextColor() {
        return mCheckedTextColor != 0 && mNormalTextColor != 0;
    }

    public int getNormalTextColor() {
        return mNormalTextColor;
    }

    public void setNormalTextColor(int normalTextColor) {
        mNormalTextColor = normalTextColor;
    }

    public int getCheckedTextColor() {
        return mCheckedTextColor;
    }

    public void setCheckedTextColor(int checkedTextColor) {
        mCheckedTextColor = checkedTextColor;
    }

    public int getRatioType() {
        return mRatioType;
    }

    public void setRatioType(int ratioType) {
        mRatioType = ratioType;
    }

    //======================================================================
    // SaveState
    //======================================================================

    public static class SavedStateViewHelper extends View.BaseSavedState {

        int checkedTextColor;
        int normalTextColor;
        double mRatio;
        boolean checked;

        public SavedStateViewHelper(Parcel source) {
            super(source);
            checkedTextColor = source.readInt();
            normalTextColor = source.readInt();
            mRatio = source.readDouble();
            checked = source.readInt() == 1;
        }

        public SavedStateViewHelper(Parcelable superState) {
            super(superState);
        }

        @Override
        public void writeToParcel(@NonNull Parcel dest, int flags) {
            super.writeToParcel(dest, flags);
            dest.writeInt(checkedTextColor);
            dest.writeInt(normalTextColor);
            dest.writeDouble(mRatio);
            dest.writeInt(checked ? 1 : 0);
        }

        public static final Creator<SavedStateViewHelper> CREATOR = new Creator<SavedStateViewHelper>() {
            public SavedStateViewHelper createFromParcel(Parcel in) {
                return new SavedStateViewHelper(in);
            }

            public SavedStateViewHelper[] newArray(int size) {
                return new SavedStateViewHelper[size];
            }
        };
    }
}
