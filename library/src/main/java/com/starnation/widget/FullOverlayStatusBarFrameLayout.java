package com.starnation.widget;

import android.annotation.SuppressLint;
import android.annotation.TargetApi;
import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Canvas;
import android.graphics.drawable.Drawable;
import android.os.Build;
import android.support.v4.view.GravityCompat;
import android.support.v4.view.ViewCompat;
import android.util.AttributeSet;
import android.view.Gravity;
import android.view.View;
import android.view.WindowInsets;
import android.widget.FrameLayout;

/*
 * @author lsh
 * @since 2015. 11. 25.
*/
public class FullOverlayStatusBarFrameLayout extends FrameLayout implements LayoutImpl {

    //======================================================================
    // Variables
    //======================================================================

    static {
        final int version = Build.VERSION.SDK_INT;
        if (version >= Build.VERSION_CODES.LOLLIPOP) {
            IMPL = new LayoutCompatImplApi21();
        } else {
            IMPL = new LayoutCompatImplBase();
        }
    }

    static final LayoutCompatImpl IMPL;

    private Object mLastInsets;

    private boolean mDrawStatusBarBackground;

    private Drawable mStatusBarBackground;

    //======================================================================
    // Constructor
    //======================================================================

    public FullOverlayStatusBarFrameLayout(Context context) {
        super(context);
        init(context);
    }

    public FullOverlayStatusBarFrameLayout(Context context, AttributeSet attrs) {
        super(context, attrs);
        init(context);
    }

    public FullOverlayStatusBarFrameLayout(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
        init(context);
    }

    private void init(Context context) {
        setFitsSystemWindows(true);
        if (ViewCompat.getFitsSystemWindows(this)) {
            IMPL.configureApplyInsets(this);
            mStatusBarBackground = IMPL.getDefaultStatusBarBackground(context);
        }
    }

    //======================================================================
    // Override Methods
    //======================================================================

    @Override
    protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
        final boolean applyInsets = mLastInsets != null && ViewCompat.getFitsSystemWindows(this);
        final int layoutDirection = ViewCompat.getLayoutDirection(this);
        final int childCount = getChildCount();

        if (childCount > 1) {
            throw new RuntimeException("child count limit 1");
        }

        for (int i = 0; i < childCount; i++) {
            final View child = getChildAt(i);

            if (child.getVisibility() == GONE) {
                continue;
            }

            final LayoutParams lp = (LayoutParams) child.getLayoutParams();

            if (applyInsets) {
                final int gravity = GravityCompat.getAbsoluteGravity(lp.gravity, layoutDirection);

                if (ViewCompat.getFitsSystemWindows(child)) {
                    IMPL.dispatchChildInsets(child, mLastInsets, gravity);
                } else {
                    IMPL.applyMarginInsets(lp, mLastInsets, gravity);
                }
            }
        }
        super.onMeasure(widthMeasureSpec, heightMeasureSpec);
    }

    @Override
    public void onDraw(Canvas c) {
        super.onDraw(c);
        if (mDrawStatusBarBackground && mStatusBarBackground != null) {
            final int inset = IMPL.getTopInset(mLastInsets);
            if (inset > 0) {
                mStatusBarBackground.setBounds(0, 0, getWidth(), inset);
                mStatusBarBackground.draw(c);
            }
        }
    }

    @Override
    public void setChildInsets(Object insets, boolean draw) {
        mLastInsets = insets;
        mDrawStatusBarBackground = draw;
        setWillNotDraw(!draw && getBackground() == null);
        requestLayout();
    }

    //======================================================================
    // Public Methods
    //======================================================================

    public Drawable getStatusBarBackground() {
        return mStatusBarBackground;
    }

    public void setStatusBarBackground(Drawable statusBarBackground) {
        if (mStatusBarBackground != statusBarBackground) {
            mStatusBarBackground = statusBarBackground;
        }
        invalidate();
    }

    //======================================================================
    // LayoutCompatImpl
    //======================================================================

    interface LayoutCompatImpl {
        void configureApplyInsets(View drawerLayout);

        void dispatchChildInsets(View child, Object insets, int gravity);

        void applyMarginInsets(MarginLayoutParams lp, Object insets, int drawerGravity);

        int getTopInset(Object lastInsets);

        Drawable getDefaultStatusBarBackground(Context context);
    }

    //======================================================================
    // LayoutCompatImplBase
    //======================================================================

    static class LayoutCompatImplBase implements LayoutCompatImpl {
        public void configureApplyInsets(View drawerLayout) {
            // This space for rent
        }

        public void dispatchChildInsets(View child, Object insets, int gravity) {
            // This space for rent
        }

        public void applyMarginInsets(MarginLayoutParams lp, Object insets, int drawerGravity) {
            // This space for rent
        }

        public int getTopInset(Object insets) {
            return 0;
        }

        @Override
        public Drawable getDefaultStatusBarBackground(Context context) {
            return null;
        }
    }

    //======================================================================
    // LayoutCompatImplApi21
    //======================================================================

    @TargetApi(Build.VERSION_CODES.LOLLIPOP)
    static class LayoutCompatImplApi21 implements LayoutCompatImpl {

        private static final int[] THEME_ATTRS = {
                android.R.attr.colorPrimaryDark
        };

        public void configureApplyInsets(View view) {
            if (view instanceof LayoutImpl) {
                view.setOnApplyWindowInsetsListener(new InsetsListener());
                view.setSystemUiVisibility(View.SYSTEM_UI_FLAG_LAYOUT_STABLE | View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN);
            }
        }

        @SuppressLint("RtlHardcoded")
        public void dispatchChildInsets(View child, Object insets, int gravity) {
            WindowInsets wi = (WindowInsets) insets;
            if (gravity == Gravity.LEFT) {
                wi = wi.replaceSystemWindowInsets(wi.getSystemWindowInsetLeft(),
                        wi.getSystemWindowInsetTop(), 0, wi.getSystemWindowInsetBottom());
            } else if (gravity == Gravity.RIGHT) {
                wi = wi.replaceSystemWindowInsets(0, wi.getSystemWindowInsetTop(),
                        wi.getSystemWindowInsetRight(), wi.getSystemWindowInsetBottom());
            }
            child.dispatchApplyWindowInsets(wi);
        }

        @SuppressLint("RtlHardcoded")
        public void applyMarginInsets(MarginLayoutParams lp, Object insets, int gravity) {
            WindowInsets wi = (WindowInsets) insets;
            if (gravity == Gravity.LEFT) {
                wi = wi.replaceSystemWindowInsets(wi.getSystemWindowInsetLeft(),
                        wi.getSystemWindowInsetTop(), 0, wi.getSystemWindowInsetBottom());
            } else if (gravity == Gravity.RIGHT) {
                wi = wi.replaceSystemWindowInsets(0, wi.getSystemWindowInsetTop(),
                        wi.getSystemWindowInsetRight(), wi.getSystemWindowInsetBottom());
            }
            lp.leftMargin = wi.getSystemWindowInsetLeft();
            lp.topMargin = wi.getSystemWindowInsetTop();
            lp.rightMargin = wi.getSystemWindowInsetRight();
            lp.bottomMargin = wi.getSystemWindowInsetBottom();
        }

        public int getTopInset(Object insets) {
            return insets != null ? ((WindowInsets) insets).getSystemWindowInsetTop() : 0;
        }

        @Override
        public Drawable getDefaultStatusBarBackground(Context context) {
            final TypedArray a = context.obtainStyledAttributes(THEME_ATTRS);
            try {
                return a.getDrawable(0);
            } finally {
                a.recycle();
            }
        }

        static class InsetsListener implements OnApplyWindowInsetsListener {
            @Override
            public WindowInsets onApplyWindowInsets(View v, WindowInsets insets) {
                final LayoutImpl drawerLayout = (LayoutImpl) v;
                drawerLayout.setChildInsets(insets, insets.getSystemWindowInsetTop() > 0);
                return insets.consumeSystemWindowInsets();
            }
        }
    }
}
