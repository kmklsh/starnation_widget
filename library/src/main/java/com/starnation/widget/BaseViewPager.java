package com.starnation.widget;

import android.content.Context;
import android.support.v4.view.ViewPager;
import android.util.AttributeSet;
import android.view.MotionEvent;

/*
 * @author lsh
 * @since 14. 12. 2.
*/
public class BaseViewPager extends ViewPager {

    //======================================================================
    // Variables
    //======================================================================

    private boolean mSwiping = true;

    //======================================================================
    // Constructor
    //======================================================================

    public BaseViewPager(Context context) {
        super(context);
    }

    public BaseViewPager(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    //======================================================================
    // Override Methods
    //======================================================================

    @Override
    public boolean onTouchEvent(MotionEvent ev) {
        if (mSwiping == false) {
            return false;
        }
        try {
            return super.onTouchEvent(ev);
        } catch (Exception ignored) {
        }
        return false;
    }

    @Override
    public boolean onInterceptTouchEvent(MotionEvent ev) {
        if (mSwiping == false) {
            return false;
        }
        try {
            return super.onInterceptTouchEvent(ev);
        } catch (Exception ignored) {
        }
        return false;
    }

    //======================================================================
    // Public Methods
    //======================================================================

    public void setSwiping(boolean swiping) {
        mSwiping = swiping;
    }
}
