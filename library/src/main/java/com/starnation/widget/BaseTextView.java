package com.starnation.widget;

import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Canvas;
import android.os.Parcel;
import android.os.Parcelable;
import android.support.annotation.NonNull;
import android.support.v4.content.ContextCompat;
import android.support.v4.widget.TextViewCompat;
import android.support.v7.widget.AppCompatTextView;
import android.text.Html;
import android.text.util.Linkify;
import android.util.AttributeSet;
import android.view.View;
import android.widget.Checkable;
import android.widget.TextView;

import com.starnation.android.text.ContentsLinkMovementMethod;

/*
 * @author lsh
 * @since 14. 12. 3.
*/
public class BaseTextView extends AppCompatTextView implements Checkable {

    //======================================================================
    // Constants
    //======================================================================

    public static final int[] CHECKED_STATE = {android.R.attr.state_checked};

    //======================================================================
    // Variables
    //======================================================================

    private OnCheckedChangeListener mCheckedChangeListener;

    private OnDrawListener mOnDrawListener;

    private ViewHelper mViewHelper = new ViewHelper();

    private boolean mDynamicMaxLine;

    private int mMaxLine;

    private boolean mCheckedEnable = true;

    private boolean mRegisterClickListener;

    //======================================================================
    // Constructor
    //======================================================================

    public BaseTextView(Context context) {
        super(context);
    }

    public BaseTextView(Context context, AttributeSet attrs) {
        super(context, attrs);
        init(context, attrs);
    }

    //======================================================================
    // Override Methods
    //======================================================================

    @Override
    public void toggle() {
        if (mCheckedChangeListener == null || mCheckedChangeListener.isMultipleChecked(this)) {
            setChecked(!isChecked());
        }
    }

    @Override
    public boolean isChecked() {
        return getViewHelper().isChecked();
    }

    @Override
    public int[] onCreateDrawableState(int extraSpace) {
        final int[] drawableState = super.onCreateDrawableState(extraSpace + 1);
        if (isChecked()) {
            mergeDrawableStates(drawableState, CHECKED_STATE);
        }
        return drawableState;
    }

    @Override
    public void setChecked(boolean checked) {
        if (mCheckedEnable == false) {
            return;
        }

        getViewHelper().setChecked(checked);
        if (getViewHelper().isCheckedTextColor() == true) {
            if (checked == true) {
                setTextColor(getViewHelper().getCheckedTextColor());
            } else {
                setTextColor(getViewHelper().getNormalTextColor());
            }
        }

        refreshDrawableState();
        if (mCheckedChangeListener != null) {
            mCheckedChangeListener.onCheckedChanged(this, checked);
        }
    }

    @Override
    protected void drawableStateChanged() {
        super.drawableStateChanged();
        TextViewUtil.updateCompoundDrawablesState(this);
        invalidate();
    }

    @Override
    protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
        if (getViewHelper().getRatio() > 0) {
            super.onMeasure(widthMeasureSpec, getViewHelper().buildMeasureSpecHeight(widthMeasureSpec));
        } else {
            super.onMeasure(widthMeasureSpec, heightMeasureSpec);
        }
    }

    @Override
    public void setOnClickListener(OnClickListener onClickListener) {
        super.setOnClickListener(new OnClickListenerWrapper(onClickListener) {
            @Override
            public void onPreClick(View v) {
                toggle();
            }
        });
        mRegisterClickListener = true;
    }

    @Override
    public Parcelable onSaveInstanceState() {
        SavedStateView state = new SavedStateView(super.onSaveInstanceState());
        state.checkedTextColor = getViewHelper().getCheckedTextColor();
        state.normalTextColor = getViewHelper().getNormalTextColor();
        state.mRatio = getViewHelper().getRatio();
        state.checked = getViewHelper().isChecked();
        return state;
    }

    @Override
    public void onRestoreInstanceState(Parcelable state) {
        if (state instanceof SavedStateView) {
            SavedStateView ss = (SavedStateView) state;
            super.onRestoreInstanceState(ss.getSuperState());
            getViewHelper().setCheckedTextColor(ss.checkedTextColor);
            getViewHelper().setNormalTextColor(ss.normalTextColor);
            getViewHelper().setRatio(ss.mRatio);
            getViewHelper().setChecked(ss.checked);
            setChecked(getViewHelper().isChecked());
            return;
        }
        super.onRestoreInstanceState(state);
    }

    @Override
    protected void onDraw(Canvas canvas) {
        if (mOnDrawListener != null) {
            mOnDrawListener.onDraw(canvas);
        }

        try {
            super.onDraw(canvas);
        } catch (StackOverflowError error) {
            postInvalidate();
        }

        if (mDynamicMaxLine == true) {
            final int dynamicMacLine = getLineCount(this);
            if (dynamicMacLine != mMaxLine) {
                mMaxLine = dynamicMacLine;
                setMaxLines(mMaxLine);
            }
        }
    }

    @Override
    protected void dispatchDraw(Canvas canvas) {
        try {
            super.dispatchDraw(canvas);
        } catch (StackOverflowError error) {
            postInvalidate();
        }
    }

    @Override
    public void setText(CharSequence text, BufferType type) {
        super.setText(text, type);
        int mask = getAutoLinkMask();
        if (mask == Linkify.WEB_URLS || mask == Linkify.ALL) {
            if (getMovementMethod() != ContentsLinkMovementMethod.getInstance()) {
                setMovementMethod(ContentsLinkMovementMethod.getInstance());
            }
        }
    }

    //======================================================================
    // Public Methods
    //======================================================================

    public void setCheckedChangeListener(OnCheckedChangeListener checkedChangeListener) {
        mCheckedChangeListener = checkedChangeListener;
        if (mRegisterClickListener == false) {
            setOnClickListener(null);
        }
    }

    public void setOnDrawListener(OnDrawListener onDrawListener) {
        mOnDrawListener = onDrawListener;
    }

    public boolean isCheckedEnable() {
        return mCheckedEnable;
    }

    public void setCheckedEnable(boolean checkedEnable) {
        mCheckedEnable = checkedEnable;
    }

    public void setRatio(double ratio) {
        getViewHelper().setRatio(ratio);
        requestLayout();
    }

    public void setCheckedTextColor(int color) {
        getViewHelper().setCheckedTextColor(color);
    }

    public void setNormalTextColor(int color) {
        getViewHelper().setNormalTextColor(color);
    }

    public boolean isDynamicMaxLine() {
        return mDynamicMaxLine;
    }

    public void setDynamicMaxLine(boolean dynamicMaxLine) {
        mDynamicMaxLine = dynamicMaxLine;
    }

    public int getSupportMaxLines() {
        if (mDynamicMaxLine == true) {
            return mMaxLine;
        }
        return TextViewCompat.getMaxLines(this);
    }

    //======================================================================
    // Private Methods
    //======================================================================

    private static int getLineCount(TextView textView) {
        final int height = textView.getMeasuredHeight() - textView.getPaddingTop() - textView.getPaddingBottom();
        return height / textView.getLineHeight();
    }

    private static int getTextLength(TextView textView) {
        final int height = textView.getMeasuredHeight() - textView.getPaddingTop() - textView.getPaddingBottom();
        return height / textView.getLineHeight();
    }

    private void init(Context context, AttributeSet attributeSet) {
        final TypedArray typedArray = context.obtainStyledAttributes(attributeSet, R.styleable.BaseTextView);
        final CharSequence charSequence = typedArray.getText(R.styleable.BaseTextView_htmlTextInput);

        if (charSequence != null) {
            this.setText(Html.fromHtml(charSequence.toString()), BufferType.SPANNABLE);
        }

        getViewHelper().setRatio(typedArray.getFloat(R.styleable.BaseTextView_ratio, 0));

        int checkedColor = typedArray.getResourceId(R.styleable.BaseTextView_checkedTextColor, 0);
        if (checkedColor != 0) {
            getViewHelper().setCheckedTextColor(ContextCompat.getColor(context, checkedColor));
        }

        int normalColor = typedArray.getResourceId(R.styleable.BaseTextView_normalTextColor, 0);
        if (normalColor != 0) {
            getViewHelper().setNormalTextColor(ContextCompat.getColor(context, normalColor));
        }

        int ratioType = typedArray.getInt(R.styleable.BaseTextView_ratioType, ViewHelper.RATIO_VERTICAL);
        getViewHelper().setRatioType(ratioType);

        typedArray.recycle();
    }

    private ViewHelper getViewHelper() {
        if (mViewHelper == null) {
            mViewHelper = new ViewHelper();
        }
        return mViewHelper;
    }

    //======================================================================
    // SaveState
    //======================================================================

    public static class SavedStateView extends BaseSavedState {

        int checkedTextColor;
        int normalTextColor;
        double mRatio;
        boolean checked;

        public SavedStateView(Parcel source) {
            super(source);
            checkedTextColor = source.readInt();
            normalTextColor = source.readInt();
            mRatio = source.readDouble();
            checked = source.readInt() == 1;
        }

        public SavedStateView(Parcelable superState) {
            super(superState);
        }

        @Override
        public void writeToParcel(@NonNull Parcel dest, int flags) {
            super.writeToParcel(dest, flags);
            dest.writeInt(checkedTextColor);
            dest.writeInt(normalTextColor);
            dest.writeDouble(mRatio);
            dest.writeInt(checked ? 1 : 0);
        }

        public static final Creator<SavedStateView> CREATOR = new Creator<SavedStateView>() {
            public SavedStateView createFromParcel(Parcel in) {
                return new SavedStateView(in);
            }

            public SavedStateView[] newArray(int size) {
                return new SavedStateView[size];
            }
        };
    }

    //======================================================================
    // OnCheckedChangeListener
    //======================================================================

    public interface OnCheckedChangeListener {
        boolean isMultipleChecked(BaseTextView textView);

        void onCheckedChanged(BaseTextView textView, boolean isChecked);
    }

    //======================================================================
    // OnDrawListener
    //======================================================================

    public interface OnDrawListener {
        void onDraw(Canvas canvas);
    }

    //======================================================================
    // OnClickListenerWrapper
    //======================================================================

    abstract static class OnClickListenerWrapper implements OnClickListener {

        OnClickListener mOnClickListener;

        public OnClickListenerWrapper(OnClickListener onClickListener) {
            mOnClickListener = onClickListener;
        }

        public abstract void onPreClick(View v);

        @Override
        public void onClick(View v) {
            onPreClick(v);
            if (mOnClickListener != null) {
                mOnClickListener.onClick(v);
            }
        }
    }
}


