package com.starnation.widget;

import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.drawable.Drawable;
import android.support.v4.content.ContextCompat;
import android.util.AttributeSet;
import android.widget.ProgressBar;

import com.starnation.android.graphics.drawable.CircularProgressDrawable;

/*
 * @author lsh
 * @since 15. 6. 29.
*/
public class CircularProgressBar extends ProgressBar {

    //======================================================================
    // Constructor
    //======================================================================

    public CircularProgressBar(Context context) {
        this(context, null);
    }

    public CircularProgressBar(Context context, AttributeSet attrs) {
        this(context, attrs, R.attr.circularProgressBar);
    }

    public CircularProgressBar(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        if (isInEditMode() == true) {
            Drawable drawable = new CircularProgressDrawable.Builder(context, true).build();
            setIndeterminateDrawable(drawable);
            return;
        }

        TypedArray a = context.obtainStyledAttributes(attrs, R.styleable.CircularProgressBar);
        float stokeWidth = a.getDimension(R.styleable.CircularProgressBar_stroke_width, getResources().getDimension(R.dimen.circular_progress_bar_default_stroke_width));
        int backgroundColor = a.getColor(R.styleable.CircularProgressBar_progress_background_color, ContextCompat.getColor(context, R.color.orange_progress_background));
        int progressColor = a.getColor(R.styleable.CircularProgressBar_progress_color, ContextCompat.getColor(context, R.color.orange_progress));
        a.recycle();

        setIndeterminateDrawable(new CircularProgressDrawable.Builder(context).
                strokeWidth(stokeWidth).
                setBackgroundColor(backgroundColor).
                color(progressColor).
                build());
    }

    //======================================================================
    // Override Methods
    //======================================================================

    @Override
    public void setVisibility(int v) {
        if (v == VISIBLE) {
            start();
        } else {
            stop();
        }
        super.setVisibility(v);
    }

    //======================================================================
    // Public Methods
    //======================================================================

    public void stop() {
        final Drawable drawable = getIndeterminateDrawable();
        if (drawable != null && drawable instanceof CircularProgressDrawable) {
            CircularProgressDrawable progressDrawable = (CircularProgressDrawable) drawable;
            progressDrawable.stop();
        }
    }

    public void start() {
        final Drawable drawable = getIndeterminateDrawable();
        if (drawable != null && drawable instanceof CircularProgressDrawable) {
            CircularProgressDrawable progressDrawable = (CircularProgressDrawable) drawable;
            progressDrawable.start();
        }
    }
}
