package com.starnation.widget.recyclerview.holder;

import android.support.annotation.LayoutRes;
import android.view.ViewGroup;

import com.starnation.widget.recyclerview.RecyclerViewHolder;
import com.starnation.widget.recyclerview.holder.selector.MultiSelector;

/*
 * @author lsh
 * @since 15. 2. 15.
*/
public class SelectorViewHolder<T> extends RecyclerViewHolder<T> {

    //======================================================================
    // Variables
    //======================================================================

    private MultiSelector mMultiSelector;

    //======================================================================
    // Constructor
    //======================================================================

    public SelectorViewHolder(ViewGroup viewGroup, @LayoutRes int layoutRes) {
        super(viewGroup, layoutRes);
    }

    //======================================================================
    // Override Methods
    //======================================================================

    @Override
    public void onRefresh(T t) {
        if (mMultiSelector != null) {
            mMultiSelector.bindHolder(this, getAdapterPosition(), getItemId());
        }
    }

    //======================================================================
    // Public Methods
    //======================================================================

    public void setMultiSelector(MultiSelector multiSelector) {
        mMultiSelector = multiSelector;
    }

    public void onChecked(boolean checked) {
        // Override
    }

    public void setChecked(boolean checked) {
        if (mMultiSelector != null) {
            mMultiSelector.setSelected(this, checked);
        }
    }
}
