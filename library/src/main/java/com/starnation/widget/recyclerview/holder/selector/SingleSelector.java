package com.starnation.widget.recyclerview.holder.selector;


import com.starnation.widget.recyclerview.holder.SelectorViewHolder;

/*
 * @author lsh
 * @since 15. 2. 15.
*/
public class SingleSelector extends MultiSelector {

    @Override
    public void setSelected(int position, boolean isSelected) {
        if (isSelected) {
            for (Integer selectedPosition : getSelectedPositions()) {
                if (selectedPosition != position) {
                    super.setSelected(selectedPosition, false);
                }
            }
        }
        super.setSelected(position, isSelected);
    }

    @Override
    public void setSelected(SelectorViewHolder holder, boolean isSelected) {
        if (isSelected(holder.getAdapterPosition()) == true) {
            return;
        }
        super.setSelected(holder, isSelected);
    }
}
