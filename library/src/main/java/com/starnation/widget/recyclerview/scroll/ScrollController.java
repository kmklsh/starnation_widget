package com.starnation.widget.recyclerview.scroll;

import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.StaggeredGridLayoutManager;

import com.starnation.widget.recyclerview.RecyclerViewAdapter;

/*
 * @author lsh
 * @since 15. 1. 5.
*/
public abstract class ScrollController extends RecyclerView.OnScrollListener {

    //======================================================================
    // Variables
    //======================================================================

    private ScrollPosition mScrollPosition;

    //======================================================================
    // Abstract Methods
    //======================================================================

    public abstract void onScrollState(State state);

    //======================================================================
    // Override Methods
    //======================================================================

    @Override
    public void onScrollStateChanged(final RecyclerView recyclerView, int newState) {
        RecyclerViewAdapter adapter = (RecyclerViewAdapter) recyclerView.getAdapter();

        final int count = adapter.getItemCount() - (adapter.getItemFooterCount() + adapter.getItemHeaderCount());

        if (mScrollPosition == null) {
            mScrollPosition = new ScrollPosition() {
                @Override
                RecyclerView.LayoutManager getLayoutManager() {
                    return recyclerView.getLayoutManager();
                }
            };
        }

        super.onScrollStateChanged(recyclerView, newState);
        switch (newState) {
            case RecyclerView.SCROLL_STATE_DRAGGING:
            case RecyclerView.SCROLL_STATE_SETTLING:
            case RecyclerView.SCROLL_STATE_IDLE:
                if (count <= 0) {
                    onScrollState(State.DRAGGING);
                    return;
                }

                final int lastItemPosition = mScrollPosition.getLastCompletelyVisibleItemPosition();
                final int firstItemPosition = mScrollPosition.getFirstCompletelyVisibleItemPosition();

                if (lastItemPosition != -1 && lastItemPosition >= count - 1) {
                    onScrollState(State.FULL_DOWN_SCROLL);
                } else if (firstItemPosition != -1 && firstItemPosition <= 0) {
                    onScrollState(State.OVER_SCROLL);
                } else {
                    onScrollState(State.DRAGGING);
                }
                break;
        }
    }

    //======================================================================
    // State
    //======================================================================

    public enum State {
        FULL_DOWN_SCROLL,
        OVER_SCROLL,
        DRAGGING,
    }

    //======================================================================
    // ScrollPosition
    //======================================================================

    static abstract class ScrollPosition {

        abstract RecyclerView.LayoutManager getLayoutManager();

        int getLastCompletelyVisibleItemPosition() {
            if (getLayoutManager() instanceof GridLayoutManager) {
                return ((GridLayoutManager) getLayoutManager()).findLastCompletelyVisibleItemPosition();
            } else if (getLayoutManager() instanceof StaggeredGridLayoutManager) {
                int arrays[] = ((StaggeredGridLayoutManager) getLayoutManager()).findLastCompletelyVisibleItemPositions(null);
                int position = 0;
                for (int array : arrays) {
                    position = Math.max(array, position);
                }
                return position;
            } else if (getLayoutManager() instanceof LinearLayoutManager) {
                return ((LinearLayoutManager) getLayoutManager()).findLastCompletelyVisibleItemPosition();
            }
            return 0;
        }

        int getFirstCompletelyVisibleItemPosition() {
            if (getLayoutManager() instanceof GridLayoutManager) {
                return ((GridLayoutManager) getLayoutManager()).findFirstCompletelyVisibleItemPosition();
            } else if (getLayoutManager() instanceof StaggeredGridLayoutManager) {
                int arrays[] = ((StaggeredGridLayoutManager) getLayoutManager()).findFirstCompletelyVisibleItemPositions(null);
                int position = 0;
                for (int array : arrays) {
                    position = Math.min(array, position);
                }
                return position;
            } else if (getLayoutManager() instanceof LinearLayoutManager) {
                return ((LinearLayoutManager) getLayoutManager()).findFirstCompletelyVisibleItemPosition();
            }
            return 0;
        }
    }
}
