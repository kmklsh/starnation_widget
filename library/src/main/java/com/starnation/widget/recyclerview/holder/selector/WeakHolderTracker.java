package com.starnation.widget.recyclerview.holder.selector;

import android.util.SparseArray;

import com.starnation.widget.recyclerview.holder.SelectorViewHolder;

import java.lang.ref.WeakReference;
import java.util.ArrayList;
import java.util.List;

/*
 * @author lsh
 * @since 15. 2. 15.
*/
public final class WeakHolderTracker {

    //======================================================================
    // Variables
    //======================================================================

    private SparseArray<WeakReference<SelectorViewHolder>> mHoldersByPosition = new SparseArray<>();

    //======================================================================
    // Public Methods
    //======================================================================

    public SelectorViewHolder getHolder(int position) {
        WeakReference<SelectorViewHolder> holderRef = mHoldersByPosition.get(position);
        if (holderRef == null) {
            return null;
        }

        SelectorViewHolder holder = holderRef.get();
        if (holder == null || holder.getAdapterPosition() != position) {
            mHoldersByPosition.remove(position);
            return null;
        }

        return holder;
    }

    public void bindHolder(SelectorViewHolder holder, int position) {
        mHoldersByPosition.put(position, new WeakReference<>(holder));
    }

    public List<SelectorViewHolder> getTrackedHolders() {
        List<SelectorViewHolder> holders = new ArrayList<>();

        for (int i = 0; i < mHoldersByPosition.size(); i++) {
            int key = mHoldersByPosition.keyAt(i);
            SelectorViewHolder holder = getHolder(key);

            if (holder != null) {
                holders.add(holder);
            }
        }

        return holders;
    }
}
