package com.starnation.widget.recyclerview.decoration;

import android.graphics.Rect;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.StaggeredGridLayoutManager;
import android.view.View;

/*
 * @author lsh
 * @since 14. 12. 25.
*/
public final class SpacingItemDecoration extends RecyclerView.ItemDecoration {

    //======================================================================
    // Constants
    //======================================================================

    public final static int VERTICAL = 1;

    //======================================================================
    // Variables
    //======================================================================

    private int mVerticalSpacing;

    private int mHorizontalSpacing;

    private int mOutsideMargin;

    //======================================================================
    // Constructor
    //======================================================================

    public SpacingItemDecoration(RecyclerView recyclerView, int verticalSpacing, int horizontalSpacing) {
        this(recyclerView, true, verticalSpacing, horizontalSpacing);
    }

    public SpacingItemDecoration(RecyclerView recyclerView, boolean horizontalFillSpacing, int verticalSpacing, int horizontalSpacing) {
        mVerticalSpacing = verticalSpacing;
        mHorizontalSpacing = horizontalSpacing;

        if (horizontalFillSpacing == true) {
            if (recyclerView.getLayoutManager() instanceof StaggeredGridLayoutManager) {
                int spanCount = ((StaggeredGridLayoutManager) recyclerView.getLayoutManager()).getSpanCount();
                if (spanCount <= 1) {
                    mHorizontalSpacing = 0;
                }
            }
        }

        if (mHorizontalSpacing > 0) {
            recyclerView.setClipToPadding(false);
            recyclerView.setPadding(mHorizontalSpacing / 2, 0, mHorizontalSpacing / 2, 0);
        }
    }

    //======================================================================
    // Override Methods
    //======================================================================

    @Override
    public void getItemOffsets(Rect outRect, View view, RecyclerView parent, RecyclerView.State state) {
        super.getItemOffsets(outRect, view, parent, state);
        final RecyclerView.LayoutManager layoutManager = parent.getLayoutManager();

        if (layoutManager instanceof StaggeredGridLayoutManager) {
            getStaggeredGridLayoutManagerItemOffsets(outRect, view, parent, state);
        } else if (layoutManager instanceof GridLayoutManager) {
            getGridLayoutManagerItemOffsets(outRect, view, parent, state);
        } else if (layoutManager instanceof LinearLayoutManager) {
            getLinearLayoutManagerItemOffsets(outRect, view, parent, state);
        }
    }

    public void getStaggeredGridLayoutManagerItemOffsets(final Rect outRect, final View view, RecyclerView parent, RecyclerView.State state) {
        final StaggeredGridLayoutManager manager = (StaggeredGridLayoutManager) parent.getLayoutManager();
        final int itemCount = manager.getItemCount();
        final int spanCount = manager.getSpanCount();
        final int position = manager.getPosition(view);
        final int orientation = manager.getOrientation();
        final boolean firstItem = position == 0;
        final boolean lastItem = position >= itemCount - 1;

        if (orientation == StaggeredGridLayoutManager.VERTICAL) {
            if (mVerticalSpacing > 0) {
                outRect.top = mVerticalSpacing;
                outRect.bottom = lastItem ? mVerticalSpacing : mOutsideMargin;
            }
            if (mHorizontalSpacing > 0) {
                outRect.left = mHorizontalSpacing / 2;
                outRect.right = mHorizontalSpacing / 2;
            }
        } else {
            if (mVerticalSpacing > 0) {
                outRect.top = mVerticalSpacing;
                outRect.bottom = mVerticalSpacing;
            }
            if (mHorizontalSpacing > 0) {
                outRect.left = firstItem ? mOutsideMargin : mHorizontalSpacing / 2;
                outRect.right = lastItem ? mOutsideMargin : mHorizontalSpacing / 2;
            }
        }
    }

    public void getGridLayoutManagerItemOffsets(Rect outRect, View view, RecyclerView parent, RecyclerView.State state) {
        final GridLayoutManager manager = (GridLayoutManager) parent.getLayoutManager();
        final int itemCount = manager.getItemCount();
        final int position = manager.getPosition(view);
        final int spanCount = manager.getSpanCount();
        final int orientation = manager.getOrientation();
        final boolean lastItem = position >= itemCount - 1;

        if (orientation == GridLayoutManager.VERTICAL) {
            if (mVerticalSpacing > 0) {
                outRect.top = mVerticalSpacing;
                outRect.bottom = lastItem ? mVerticalSpacing : mOutsideMargin;
            }
            if (mHorizontalSpacing > 0) {
                outRect.left = mHorizontalSpacing;
                if (spanCount > 1 && lastItem == false && position % spanCount == 0) {
                    outRect.right = mOutsideMargin;
                } else {
                    outRect.right = mHorizontalSpacing;
                }
            }
        } else {
            // Modify 15. 1. 17. lsh 가로 는 현재 사용 안함 그래서 구현 안함 ㅋㅋ
        }
    }

    public void getLinearLayoutManagerItemOffsets(Rect outRect, View view, RecyclerView parent, RecyclerView.State state) {
        final LinearLayoutManager manager = (LinearLayoutManager) parent.getLayoutManager();
        final int itemCount = manager.getItemCount();
        final int position = manager.getPosition(view);
        final int orientation = manager.getOrientation();
        final boolean lastItem = position >= itemCount - 1;

        if (orientation == LinearLayoutManager.VERTICAL) {
            if (mVerticalSpacing > 0) {
                outRect.top = mVerticalSpacing / 2;
                outRect.bottom = lastItem ? mVerticalSpacing / 2 : 0;
            }
            if (mHorizontalSpacing > 0) {
                outRect.left = mHorizontalSpacing / 2;
                outRect.right = mHorizontalSpacing / 2;
            }
        } else {
            if (mVerticalSpacing > 0) {
                outRect.top = mVerticalSpacing / 2;
                outRect.bottom = lastItem ? mVerticalSpacing / 2 : mOutsideMargin;
            }
            if (mHorizontalSpacing > 0) {
                outRect.left = mHorizontalSpacing / 2;
                outRect.right = mHorizontalSpacing / 2;
            }
        }
    }

    public int getVerticalSpacing() {
        return mVerticalSpacing;
    }

    public int getHorizontalSpacing() {
        return mHorizontalSpacing;
    }

    public void setOutsideMargin(int outsideMargin) {
        mOutsideMargin = outsideMargin;
    }
}
