package com.starnation.widget.recyclerview;

import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.View;

/*
 * @author lsh
 * @since 15. 2. 10.
*/
public final class AdapterUtil {

    public static void releaseRecyclerViewHolder(SwipeRefreshRecyclerView recyclerView) {
        if (recyclerView != null && recyclerView.getRecyclerView() != null) {
            releaseRecyclerViewHolder(recyclerView.getRecyclerView());
        }
    }

    public static void releaseRecyclerViewHolder(@NonNull RecyclerView recyclerView) {
        final int viewCount = recyclerView.getChildCount();
        for (int i = 0; i < viewCount; i++) {
            final View view = recyclerView.getChildAt(i);
            final RecyclerView.ViewHolder holder = recyclerView.getChildViewHolder(view);
            final RecyclerViewHolder viewHolder = (RecyclerViewHolder) holder;
            viewHolder.onViewRecycled();
        }
    }

    public static void releaseAdapter(SwipeRefreshRecyclerView recyclerView) {
        if (recyclerView != null && recyclerView.getRecyclerView() != null) {
            RecyclerViewAdapter adapter = recyclerView.getAdapter();
            if (adapter != null) {
                adapter.onDetachedFromRecyclerView(recyclerView.getRecyclerView());
            }
        }
    }

    public static void notifySupportDataSetChanged(RecyclerViewAdapter adapter, boolean notifyDataSetChanged) {
        if (adapter == null) {
            return;
        }

        adapter.notifySupportRemoveFooter();
        adapter.notifySupportRemoveEmptyView();

        if (notifyDataSetChanged == true) {
            adapter.insertEmptyView();
            adapter.notifyDataSetChanged();
        } else {
            if (adapter.insertEmptyView() == true) {
                adapter.notifyDataSetChanged();
            }
        }
    }

    /**
     * If you delete multiple items, use the following : {@link RecyclerView.setNestedScrollingEnabled(boolean)}
     * @param adapter @link {@link RecyclerViewAdapter}
     * @param position Adapter position
     */
    @SuppressWarnings("JavadocReference")
    public static void notifyItemRemoved(RecyclerViewAdapter adapter, int position) {
        if (adapter == null || position == -1) {
            return;
        }

        try {
            adapter.notifyItemRemoved(position);
        } catch (Exception e) {
            //Nothing
        }
    }
}
