package com.starnation.widget.recyclerview.decoration;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Rect;
import android.graphics.drawable.Drawable;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.StaggeredGridLayoutManager;
import android.view.View;

import com.starnation.widget.R;


/*
 * @author lsh
 * @since 14. 12. 24.
*/
public class DividerItemDecoration extends RecyclerView.ItemDecoration {

    //======================================================================
    // Constants
    //======================================================================

    public static final int HORIZONTAL = 1;

    public static final int VERTICAL = 2;

    //======================================================================
    // Variables
    //======================================================================

    private Drawable mDivider;

    private int mOrientation;

    private int mLeftPadding;

    private int mRightPadding;

    private boolean mLastDiver;

    //======================================================================
    // Constructor
    //======================================================================

    public DividerItemDecoration(Context context, int orientation) {
        mDivider = ContextCompat.getDrawable(context, R.drawable.widget_list_line);
        setOrientation(orientation);
    }

    public DividerItemDecoration(Context context, int orientation, boolean lastDiver) {
        mDivider = ContextCompat.getDrawable(context, R.drawable.widget_list_line);
        mLastDiver = lastDiver;
        setOrientation(orientation);
    }

    public DividerItemDecoration(Drawable divider, int orientation, boolean lastDiver) {
        mDivider = divider;
        mLastDiver = lastDiver;
        setOrientation(orientation);
    }

    //======================================================================
    // Override Methods
    //======================================================================

    @Override
    public void onDraw(Canvas c, RecyclerView parent, RecyclerView.State state) {
        if (mOrientation == VERTICAL) {
            drawVertical(c, parent);
        } else {
            drawHorizontal(c, parent);
        }
    }

    @Override
    public void getItemOffsets(Rect outRect, View view, RecyclerView parent, RecyclerView.State state) {
        int position = -1;
        if (parent.getLayoutManager() instanceof StaggeredGridLayoutManager) {
            final StaggeredGridLayoutManager manager = (StaggeredGridLayoutManager) parent.getLayoutManager();
            position = manager.getPosition(view);
        } else if (parent.getLayoutManager() instanceof GridLayoutManager) {
            final GridLayoutManager manager = (GridLayoutManager) parent.getLayoutManager();
            position = manager.getPosition(view);
        }

        if (mOrientation == VERTICAL) {
            outRect.set(getHorizontalSpacing(position), 0, getHorizontalSpacing(position), mDivider.getIntrinsicHeight());
        } else {
            outRect.set(0, 0, mDivider.getIntrinsicWidth(), 0);
        }
    }

    //======================================================================
    // Public Methods
    //======================================================================

    public void setOrientation(int orientation) {
        if (orientation != HORIZONTAL && orientation != VERTICAL) {
            throw new IllegalArgumentException("invalid orientation");
        }
        mOrientation = orientation;
    }

    public void setPadding(int padding) {
        mLeftPadding = mRightPadding = padding;
    }

    public void setPadding(int left, int right) {
        mLeftPadding = left;
        mRightPadding = right;
    }

    public int getVerticalLeftSpacing(int position) {
        return mLeftPadding;
    }

    public int getVerticalRightSpacing(int position) {
        return mRightPadding;
    }

    public int getHorizontalSpacing(int position) {
        return 0;
    }

    public int getVerticalSpacing(int position) {
        return 0;
    }

    //======================================================================
    // Private Methods
    //======================================================================

    private void drawVertical(Canvas c, RecyclerView parent) {
        final int left = parent.getPaddingLeft();
        final int right = parent.getWidth() - parent.getPaddingRight();
        final int childCount = parent.getChildCount();

        final int listCount = mLastDiver ? childCount : childCount - 1;

        if (childCount > 0) {
            for (int i = 0; i < listCount; i++) {
                final View child = parent.getChildAt(i);
                final RecyclerView.LayoutParams params = (RecyclerView.LayoutParams) child
                        .getLayoutParams();
                final int top = child.getBottom() + params.bottomMargin;
                final int bottom = top + 2;
                c.save();
                int position = parent.getChildViewHolder(child).getAdapterPosition();
                mDivider.setBounds(left + getVerticalLeftSpacing(position), top, right - getVerticalRightSpacing(position), bottom);
                mDivider.draw(c);
                c.restore();
            }
        }
    }

    private void drawHorizontal(Canvas c, RecyclerView parent) {
        final int top = parent.getPaddingTop();
        final int bottom = parent.getHeight() - parent.getPaddingBottom();
        final int childCount = parent.getChildCount();
        final int listCount = mLastDiver ? childCount : childCount - 1;

        if (childCount > 0) {
            for (int i = 0; i < listCount; i++) {
                final View child = parent.getChildAt(i);
                final RecyclerView.LayoutParams params = (RecyclerView.LayoutParams) child
                        .getLayoutParams();
                final int left = child.getRight() + params.rightMargin;
                final int right = left + mDivider.getIntrinsicHeight();
                c.save();
                mDivider.setBounds(left, top, right, bottom);
                mDivider.draw(c);
                c.restore();
            }
        }
    }
}
