package com.starnation.widget.recyclerview;

import android.content.Context;
import android.content.res.Resources;
import android.os.Bundle;
import android.support.annotation.LayoutRes;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.starnation.widget.BaseImageView;

/*
 * @author lsh
 * @since 14. 12. 23.
*/
@SuppressWarnings("ALL")
public abstract class RecyclerViewHolder<Item> extends KotlinRecyclerViewHolder {

    //======================================================================
    // Variables
    //======================================================================

    private final View mItemView;

    private Item mItem;

    private boolean mItemChange;

    private boolean mAutoImageRelease = true;

    Bundle mArguments;

    //======================================================================
    // Constructor
    //======================================================================

    public RecyclerViewHolder(@NonNull ViewGroup viewGroup, @LayoutRes int itemVieId) {
        this(LayoutInflater.from(viewGroup.getContext()).inflate(itemVieId, viewGroup, false));
    }

    public RecyclerViewHolder(@NonNull SwipeRefreshRecyclerView viewGroup, @LayoutRes int itemVieId) {
        this(buildItemView(viewGroup.getRecyclerView(), itemVieId));
    }

    public RecyclerViewHolder(@NonNull View itemView) {
        super(itemView);
        mItemView = itemView;
        onCreateView(itemView);
    }

    //======================================================================
    // Abstarct Methods
    //======================================================================

    public abstract void onRefresh(Item item);

    //======================================================================
    // Methods
    //======================================================================

    /**
     * {@link RecyclerViewAdapter#onViewAttachedToWindow(RecyclerViewHolder)} 호출시 {@link #onViewAttachedToWindow()} 을 호출 한다.
     */
    final void onViewAttachedToWindow() {
        // Nothing
    }

    /**
     * {@link RecyclerViewAdapter#onViewDetachedFromWindow(RecyclerViewHolder)} 호출시 {@link #onViewDetachedFromWindow()} 을 호출 한다.
     */
    final void onViewDetachedFromWindow() {
        // Nothing
    }

    /**
     * {@link RecyclerViewAdapter#onBindViewHolder(RecyclerViewHolder, int)} 호출시 {@link #onBindViewHolder(Item)} 을 호출 한다.
     *
     */
    void onPreBindViewHolder() {
        onBindArguments(getArgumentsInternal());
    }

    /**
     * {@link RecyclerViewAdapter#onBindViewHolder(RecyclerViewHolder, int)} 호출시 {@link #onPreBindViewHolder()} 을 호출 한다.
     *
     * @param item {@link RecyclerViewAdapter#getItem(int)}
     */
    void onBindViewHolder(Item item) {
        onBindArguments(getArgumentsInternal());
        if (mItem == null || mItem.equals(item) == false) {
            mItem = item;
            mItemChange = true;
        } else {
            mItemChange = false;
        }

        onBind();
        onRefresh(item);
    }

    /**
     * {@link RecyclerViewAdapter#onViewRecycled(RecyclerViewHolder)} 호출시 {@link #release()} 을 호출 한다.
     */
    final void onViewRecycled() {
        mItem = null;
        release();
    }

    /**
     * {@link RecyclerViewAdapter#onCreateViewHolder(ViewGroup, int)} 호출
     *
     * @param viewType
     */
    final void onCreateViewHolder(int viewType) {

    }

    //======================================================================
    // Protected Methods
    //======================================================================

    /**
     * {@link RecyclerViewHolder} View 생성시 호출
     *
     * @param view
     *
     * @see RecyclerViewAdapter#onCreateViewHolder(ViewGroup, int)
     */
    protected void onCreateView(@NonNull View view) {

    }

    /**
     * {@link #onBindViewHolder(Object)} 연결시 호출
     *
     * @see {@link RecyclerViewAdapter#onBindViewHolder(RecyclerViewHolder, int)}
     */
    protected void onBind() {
        // Override
    }

    /**
     * {@link RecyclerViewHolder} 연결시 전달할 인자값
     * <p>{@link #onBind()} 보다 먼저 호출된다.</p>
     *
     * @param arguments {@link Bundle 전달된 값}
     */
    protected void onBindArguments(@NonNull Bundle arguments) {

    }

    //======================================================================
    // Public Methods
    //======================================================================

    public static View buildItemView(RecyclerView recyclerView, int itemViewId) {
        return LayoutInflater.from(recyclerView.getContext()).inflate(itemViewId, recyclerView, false);
    }

    public final Item getItem() {
        return mItem;
    }

    public boolean isItemChange() {
        return mItemChange;
    }

    /**
     * ViewHolder에 {@link BaseImageView}가 포함되어 있으면 해제 여부
     *
     * @return true 자동 해제
     */
    public boolean isAutoImageRelease() {
        return mAutoImageRelease;
    }

    /**
     * ViewHolder에 {@link BaseImageView}가 포함되어 있으면 해제 여부
     *
     * @return true 자동 해제
     */
    public void setAutoImageRelease(boolean autoImageRelease) {
        mAutoImageRelease = autoImageRelease;
    }

    /**
     * <p>기본 반환 값은 {@link #getItem()} 을 반환 한다.</p>
     * <p>position 값은 {@link #addChildViewHolder(RecyclerViewHolder)} 을 통해 등록된 {@link RecyclerViewHolder} position 이다. </p>
     * <p>position {@link RecyclerViewHolder} 에 다른 반환 값을 지정 을 {@link Override} 하여 구현을 할수 있다. <p/>
     *
     * @param position {@link RecyclerViewHolder Child RecyclerViewHolder } 위치
     *
     * @return {@link RecyclerViewHolder Child RecyclerViewHolder} Item 을 반환 한다.
     */
    @Nullable
    public Object onChildRecyclerViewHolderBindItem(int position) {
        return mItem;
    }

    public final Resources getResources() {
        return getContext().getResources();
    }

    public final Context getContext() {
        return mItemView.getContext();
    }

    public final View getItemView() {
        return mItemView;
    }

    public final View findViewById(int id) {
        return mItemView.findViewById(id);
    }

    @NonNull
    public final Bundle getArguments() {
        return getArgumentsInternal();
    }

    /**
     * 리소스 해제
     */
    public void release() {
        release(mItemView);
    }

    //======================================================================
    // Private Methods
    //======================================================================

    @NonNull
    public Bundle getArgumentsInternal() {
        if (mArguments == null) {
            mArguments = new Bundle();
        }
        return mArguments;
    }

    private void release(View rootView) {
        if (rootView == null) {
            return;
        }

        if (rootView instanceof ViewGroup) {
            ViewGroup groupView = (ViewGroup) rootView;

            int childCount = groupView.getChildCount();
            for (int index = 0; index < childCount; index++) {
                release(groupView.getChildAt(index));
            }
        } else if (rootView instanceof BaseImageView && mAutoImageRelease == true) {
            BaseImageView imageView = (BaseImageView) rootView;
            if (imageView.isUrlTag() == true) {
                imageView.releaseImage();
            }
        }
    }
}
