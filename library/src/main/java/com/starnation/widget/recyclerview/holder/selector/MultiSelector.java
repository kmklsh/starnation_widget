package com.starnation.widget.recyclerview.holder.selector;

import android.util.SparseBooleanArray;

import com.starnation.util.validator.CollectionValidator;
import com.starnation.widget.recyclerview.holder.SelectorViewHolder;

import java.util.ArrayList;
import java.util.List;

/*
 * @author lsh
 * @since 15. 2. 15.
*/
public class MultiSelector {

    //======================================================================
    // Variables
    //======================================================================

    private SparseBooleanArray mSelections = new SparseBooleanArray();

    private WeakHolderTracker mTracker = new WeakHolderTracker();

    @Deprecated
    private boolean mIsSelectable = true;

    private int mMinSelectCount = 0;

    //======================================================================
    // Public Methods
    //======================================================================

    @Deprecated
    public void setSelectable(boolean isSelectable) {
        mIsSelectable = isSelectable;
        refreshAllHolders();
    }

    public boolean isSelectable() {
        return mIsSelectable;
    }

    public void setSelected(SelectorViewHolder holder, boolean isSelected) {
        if (enableCount(isSelected) == true) {
            setSelected(holder.getAdapterPosition(), isSelected);
        }
    }

    public int getMinSelectCount() {
        return mMinSelectCount;
    }

    public void setMinSelectCount(int minSelectCount) {
        mMinSelectCount = minSelectCount;
    }

    public void setSelected(int position, boolean isSelected) {
        mSelections.put(position, isSelected);
        refreshHolder(mTracker.getHolder(position));
    }

    public boolean isSelected(int position) {
        return mSelections.get(position);
    }

    public void clearSelections() {
        mSelections.clear();
        refreshAllHolders();
    }

    public List<Integer> getSelectedPositions() {
        List<Integer> positions = new ArrayList<Integer>();

        for (int i = 0; i < mSelections.size(); i++) {
            if (mSelections.valueAt(i)) {
                positions.add(mSelections.keyAt(i));
            }
        }

        return positions;
    }

    public void bindHolder(SelectorViewHolder holder, int position, long id) {
        mTracker.bindHolder(holder, position);
        refreshHolder(holder);
    }

    public boolean tapSelection(SelectorViewHolder holder) {
        return tapSelection(holder.getAdapterPosition());
    }

    public boolean tapSelection(int position) {
        if (mIsSelectable) {
            boolean isSelected = isSelected(position);
            setSelected(position, !isSelected);
            return true;
        } else {
            return false;
        }

    }

    private void refreshAllHolders() {
        for (SelectorViewHolder holder : mTracker.getTrackedHolders()) {
            refreshHolder(holder);
        }
    }

    private void refreshHolder(SelectorViewHolder holder) {
        if (holder == null) {
            return;
        }

        boolean isActivated = mSelections.get(holder.getAdapterPosition());
        holder.getItemView().setActivated(isActivated);
        holder.onChecked(isActivated);
    }

    private boolean enableCount(boolean isSelected) {
        return (CollectionValidator.getSize(getSelectedPositions()) <= mMinSelectCount && isSelected == false) == false;
    }
}
