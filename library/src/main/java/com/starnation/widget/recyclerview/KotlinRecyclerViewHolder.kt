package com.starnation.widget.recyclerview

import android.support.v7.widget.RecyclerView
import android.view.View

import kotlinx.android.extensions.LayoutContainer

/*
 * @author lsh
 * @since 14. 12. 23.
*/
abstract class KotlinRecyclerViewHolder(override val containerView: View) : RecyclerView.ViewHolder(containerView), LayoutContainer
