package com.starnation.widget;

/*
 * @author lsh
 * @since 15. 6. 14.
*/
public enum WidgetTag {
    WEB_VIEW,
    DEFAULT;

    //======================================================================
    // Override Methods
    //======================================================================

    @Override
    public String toString() {
        return super.toString();
    }

    //======================================================================
    // Public Methods
    //======================================================================

    public String toTag(){
        return toString();
    }

    public boolean isEnable() {
        return true;
    }
}
