package com.starnation.widget;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Rect;
import android.support.annotation.NonNull;
import android.util.AttributeSet;
import android.util.TypedValue;
import android.view.Gravity;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.TextView;

import com.starnation.android.view.ViewUtil;

import java.util.ArrayList;

/*
 * @author lsh
 * @since 15. 3. 29.
*/
@SuppressWarnings("unused")
public class AutoColumnLayout extends FrameLayout {

    //======================================================================
    // Variables
    //======================================================================

    private int mVerticalSpacing;

    private int mHorizontalSpacing;

    private int mMaxRowCount;

    @SuppressLint("RtlHardcoded")
    private int mGravity = Gravity.LEFT;

    private ArrayList<FlexColumn> mFlexColumnList = new ArrayList<>();

    //======================================================================
    // Constructor
    //======================================================================

    public AutoColumnLayout(Context context) {
        super(context);
    }

    public AutoColumnLayout(Context context, AttributeSet attrs) {
        this(context, attrs, 0);
    }

    @SuppressLint("RtlHardcoded")
    public AutoColumnLayout(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);

        TypedArray t = context.obtainStyledAttributes(attrs, R.styleable.AutoColumnLayout);
        mVerticalSpacing = t.getDimensionPixelSize(R.styleable.AutoColumnLayout_verticalSpacing, 0);
        mHorizontalSpacing = t.getDimensionPixelSize(R.styleable.AutoColumnLayout_horizontalSpacing, 0);
        mMaxRowCount = t.getInteger(R.styleable.AutoColumnLayout_maxRowCount, 0);

        switch(t.getInt(R.styleable.AutoColumnLayout_autocolumnGravity, 0)) {
            case 0:
                mGravity = Gravity.LEFT;
                break;
            case 1:
                mGravity = Gravity.RIGHT;
                break;
            case 2:
                mGravity = Gravity.CENTER;
                break;
        }

        t.recycle();
    }

    //======================================================================
    // Override Methods
    //======================================================================

    @SuppressWarnings("PointlessBooleanExpression")
    @Override
    protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
        super.onMeasure(widthMeasureSpec, heightMeasureSpec);

        mFlexColumnList.clear();
        FlexColumn flexColumn = null;
        for (int i = 0; i < getChildCount(); i++) {
            final View view = getChildAt(i);

            if (flexColumn == null || flexColumn.isAddEnable(getMeasuredWidth(), view) == false) {
                flexColumn = new FlexColumn(mVerticalSpacing, mHorizontalSpacing);
                mFlexColumnList.add(flexColumn);
            }
            flexColumn.addItem(view);
        }

        final int limitLineCount = mMaxRowCount == 0 ?
                mFlexColumnList.size() : Math.min(mFlexColumnList.size(), mMaxRowCount);
        int listHeight = 0;
        for (int i = 0; i < limitLineCount; i++) {
            listHeight += mFlexColumnList.get(i).toHeight();
            if (i < limitLineCount - 1) {
                listHeight += mVerticalSpacing;
            }
        }
        setMeasuredDimension(MeasureSpec.getSize(widthMeasureSpec), listHeight);
    }

    @Override
    protected void onLayout(boolean changed, int l, int t, int r, int b) {
        gravityLayout();
    }

    //======================================================================
    // Public Methods
    //======================================================================

    public void setGravity(int gravity) {
        mGravity = gravity;
    }

    public void setMaxRowCount(int maxRowCount) {
        if (mMaxRowCount != maxRowCount) {
            mMaxRowCount = maxRowCount;
            requestLayout();
        }
    }

    @SuppressWarnings("unchecked")
    public void setAdapter(Adapter adapter) {
        if (adapter != null) {
            final int childCount = adapter.getCount();
            if (childCount > 0) {
                if (getChildCount() > 0) {
                    removeAllViews();
                }
                for (int i = 0; i < childCount; i++) {
                    addView(adapter.onCreateView(this, i, adapter.getItem(i)));
                }
            }
        }
    }

    @SuppressWarnings("PointlessBooleanExpression")
    public void notifyChildView(Iterator iterator) {
        if (iterator != null) {
            final int childCount = getChildCount();
            for (int i = 0; i < childCount; i++) {
                View view = getChildAt(i);
                if (iterator.isContinue(i, view) == true) {
                    continue;
                }
                iterator.onView(i, view);

                if (iterator.isNext(i, view) == false) {
                    break;
                }
            }
        }
    }

    @SuppressWarnings("PointlessBooleanExpression")
    public View findViewByCompare(Compare compare) {
        if (compare != null) {
            final int childCount = getChildCount();
            for (int i = 0; i < childCount; i++) {
                View view = getChildAt(i);
                if (compare.findView(i, view) == true) {
                    return view;
                }
            }
        }
        return null;
    }

    //======================================================================
    // Private Methods
    //======================================================================

    private void gravityLayout() {
        final int limitLineCount = mMaxRowCount == 0 ?
                mFlexColumnList.size() : Math.min(mFlexColumnList.size(), mMaxRowCount);

        int itemIndex = 0;
        int top = 0;
        for (int line = 0; line < mFlexColumnList.size(); line++) {
            try {
                FlexColumn flexColumn = mFlexColumnList.get(line);
                int left = calculationStartOffset(flexColumn);
                for (int item = 0; item < flexColumn.mItemList.size(); item++) {
                    final View view = getChildAt(itemIndex);
                    if (line < limitLineCount) {
                        ViewUtil.show(view);

                        setLayout(view,
                                left,
                                top + calculationTopOffset(flexColumn, item),
                                0,
                                top + calculationTopOffset(flexColumn, item) + view.getMeasuredHeight());
                        left += mHorizontalSpacing;
                        left += view.getMeasuredWidth();
                    } else {
                        ViewUtil.hide(view);
                    }
                    itemIndex++;
                }

                top += mVerticalSpacing;
                top += flexColumn.toHeight();
            } catch (Exception e) {
                //Nothing
            }
        }
    }

    @SuppressLint("RtlHardcoded")
    private int calculationStartOffset(FlexColumn flexColumn) {
        if (flexColumn == null) {
            return 0;
        }

        switch(mGravity) {
            case Gravity.CENTER:
            case Gravity.CENTER_HORIZONTAL:
                return (getMeasuredWidth() - flexColumn.toWidth()) / 2;
            case Gravity.RIGHT:
                return getMeasuredWidth() - flexColumn.toWidth();
            default:
                return 0;
        }
    }

    private int calculationTopOffset(FlexColumn flexColumn, int item) {
        if (flexColumn == null) {
            return 0;
        }

        switch(mGravity) {
            case Gravity.CENTER:
            case Gravity.CENTER_VERTICAL:
                return (flexColumn.toHeight() - flexColumn.mItemList.get(item).height()) / 2;
            case Gravity.BOTTOM:
                return flexColumn.toHeight() - flexColumn.mItemList.get(item).height();
            default:
                return 0;
        }
    }

    private void setLayout(View view, int left, int top, int right, int bottom) {
        if (view.getMeasuredWidth() > getMeasuredWidth()) {
            if (view instanceof TextView) {
                ((TextView) view).setTextSize(TypedValue.COMPLEX_UNIT_PX, getResources().getDimensionPixelSize(R.dimen.lib_text_size_14dip));
            }
        }

        int width = Math.min(view.getMeasuredWidth(), getMeasuredWidth() - (mVerticalSpacing * 2));

        if (right <= 0) {
            right = left + width;
        }
        view.layout(left, top, right, bottom);
    }

    //======================================================================
    // Compare
    //======================================================================

    @SuppressWarnings("WeakerAccess")
    public interface Compare {

        /**
         * 비교 대상과 동일 여부 비교
         *
         * @param position 뷰 위치
         * @param view     {@link View Child View}
         *
         * @return 동일하면 true 아니면 false
         */
        boolean findView(int position, View view);
    }

    //======================================================================
    // Iterator
    //======================================================================

    @SuppressWarnings("WeakerAccess")
    public static abstract class Iterator {
        public boolean isNext(int position, View view) {
            return true;
        }

        public boolean isContinue(int position, View view) {
            return false;
        }

        public abstract void onView(int position, View view);
    }

    //======================================================================
    // Adapter
    //======================================================================

    public interface Adapter<Item> {
        int getCount();

        Item getItem(int position);

        View onCreateView(@NonNull AutoColumnLayout layout, int position, Item item);
    }

    //======================================================================
    // class FlexColumn
    //======================================================================

    private class FlexColumn {

        //======================================================================
        // Variables
        //======================================================================

        private ArrayList<Rect> mItemList = new ArrayList<>();

        private int mVerticalSpacing;

        private int mHorizontalSpacing;

        //======================================================================
        // Constructor
        //======================================================================

        FlexColumn(int verticalSpacing, int horizontalSpacing) {
            mVerticalSpacing = verticalSpacing;
            mHorizontalSpacing = horizontalSpacing;
        }

        //======================================================================
        // Public Methods
        //======================================================================

        //Modify 2017. 7. 24. vkwofm 라인에 대한 체크만 한다.
        boolean isAddEnable(int limitSize, View view) {
            return mHorizontalSpacing + view.getMeasuredWidth() + toWidth() < limitSize;
        }

        void addItem(View view) {
            mItemList.add(new Rect(0, 0, view.getMeasuredWidth(), view.getMeasuredHeight()));
        }

        int toWidth() {
            int width = 0;
            for (int i = 0; i < mItemList.size(); i++) {
                width += mItemList.get(i).width();
                if (i < mItemList.size() - 1) {
                   width += mHorizontalSpacing;
                }
            }
            return width;
        }

        @SuppressWarnings("UnusedAssignment")
        int toHeight() {
            int height = 0;
            for (int i = 0; i < mItemList.size(); i++) {
                height = Math.max(height, mItemList.get(i).height());
            }
            return height;
        }
    }
}
