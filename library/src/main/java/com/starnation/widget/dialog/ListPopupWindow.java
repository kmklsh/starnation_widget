package com.starnation.widget.dialog;

import android.content.Context;
import android.graphics.drawable.Drawable;
import android.util.AttributeSet;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;

import com.starnation.widget.R;
import com.starnation.widget.listview.BaseAdapter;

/*
 * @author lsh
 * @since 14. 12. 17.
*/
public final class ListPopupWindow extends android.support.v7.widget.ListPopupWindow {

    //======================================================================
    // Variables
    //======================================================================

    private AdapterView.OnItemClickListener mOnItemClickListener;

    //======================================================================
    // Constructor
    //======================================================================

    public static ListPopupWindow create(Context context, BaseAdapter baseAdapter, AdapterView.OnItemClickListener onItemClickListener) {
        final ListPopupWindow listPopupWindow = new ListPopupWindow(context);
        listPopupWindow.setAdapter(baseAdapter);
        listPopupWindow.setOnItemClickListener(onItemClickListener);
        return listPopupWindow;
    }

    public ListPopupWindow(Context context, AttributeSet attrs, int defStyleAttr, int defStyleRes) {
        super(context, attrs, defStyleAttr, defStyleRes);
    }

    public ListPopupWindow(Context context) {
        super(context);
        init(context);
    }

    public ListPopupWindow(Context context, AttributeSet attrs) {
        super(context, attrs);
        init(context);
    }

    //======================================================================
    // Override Methods
    //======================================================================

    @Override
    public void show() {
        super.show();
        final ListView listView = getListView();
        if (listView != null) {
            listView.setOverScrollMode(View.OVER_SCROLL_NEVER);
            listView.setVerticalScrollBarEnabled(false);
            listView.setChoiceMode(ListView.CHOICE_MODE_SINGLE);
        }
    }

    @Override
    public void setOnItemClickListener(AdapterView.OnItemClickListener clickListener) {
        mOnItemClickListener = clickListener;
    }

    @Override
    public void setBackgroundDrawable(Drawable d) {
        super.setBackgroundDrawable(d);
    }

    //======================================================================
    // Public Methods
    //======================================================================

    public void show(View anchorView) {
        setAnchorView(anchorView);
        show();
    }

    //======================================================================
    // Private Methods
    //======================================================================

    private void init(Context context) {
        setModal(true);
        setHorizontalOffset(context.getResources().getDimensionPixelSize(R.dimen.lib_drop_horizontal_offset));
        setVerticalOffset(context.getResources().getDimensionPixelSize(R.dimen.lib_drop_down_vertical_offset));
        setWidth(WRAP_CONTENT);
        setHeight(WRAP_CONTENT);
        super.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                dismiss();
                if (mOnItemClickListener != null) {
                    mOnItemClickListener.onItemClick(parent, view, position, id);
                }
            }
        });
    }
}
