package com.starnation.widget;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.res.Resources;
import android.content.res.TypedArray;
import android.graphics.Color;
import android.graphics.drawable.Drawable;
import android.graphics.drawable.TransitionDrawable;
import android.support.annotation.ColorInt;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.widget.AppCompatImageView;
import android.util.AttributeSet;
import android.util.DisplayMetrics;
import android.util.SparseArray;

import com.bumptech.glide.load.DataSource;
import com.bumptech.glide.load.Transformation;
import com.bumptech.glide.load.engine.GlideException;
import com.bumptech.glide.load.resource.gif.GifDrawable;
import com.bumptech.glide.request.target.Target;
import com.starnation.android.view.ViewUtil;
import com.starnation.glide.GlideLoader;
import com.starnation.glide.transformation.BlurBitmapTransformation;
import com.starnation.glide.transformation.RoundedBitmapTransformation;
import com.starnation.util.StringUtil;

/*
 * @author lsh
 * @since 14. 12. 12.
 */
public class BaseImageView extends AppCompatImageView {

    //======================================================================
    // Constants
    //======================================================================

    public static final int RATIO_VERTICAL = 1;

    public static final int RATIO_HORIZONTAL = 2;

    private final static int KEY_BLUR = 1;

    private final static int KEY_ROUNDED = 1;

    private final static String FORMAT_TRANSFORMATION = "TRANSFORMATION_%d";

    //======================================================================
    // Variables
    //======================================================================

    private SparseArray<Transformation> mTransformationSparseArray = new SparseArray<>();

    private GlideLoader mGlideLoader;

    private float mRatio;

    private int mRatioType = RATIO_VERTICAL;

    private RequestListenerWrap mListener = new RequestListenerWrap();

    private TransformationOption mOption = new TransformationOption();

    private boolean mAutoStartGifDrawable = false;

    //======================================================================
    // Constructor
    //======================================================================

    public BaseImageView(Context context) {
        super(context);
        init(context, null);
    }

    public BaseImageView(Context context, AttributeSet attrs) {
        super(context, attrs);
        init(context, attrs);
    }

    public BaseImageView(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
        init(context, attrs);
    }

    private void init(Context context, AttributeSet attrs) {
        if (context == null) {
            return;
        }

        if (attrs != null) {
            TypedArray a = context.obtainStyledAttributes(attrs, R.styleable.BaseImageView);
            mRatio = a.getFloat(R.styleable.BaseImageView_ratio, 0f);
            setErrorImage(a.getResourceId(R.styleable.BaseImageView_error_image, 0));
            setDefaultImage(a.getResourceId(R.styleable.BaseImageView_default_image, R.drawable.drawable_translate_image));
            mRatioType = a.getInt(R.styleable.BaseImageView_ratioType, RATIO_VERTICAL);
            mOption.init(context, attrs);
            a.recycle();
        }
        glideLoader().override(ImageSize.NORMAL.mMaxSize, ImageSize.NORMAL.mMaxSize);
    }

    //======================================================================
    // Override Methods
    //======================================================================

    @Override
    protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
        if (mRatio > 0) {
            switch(mRatioType) {
                case RATIO_VERTICAL:
                    super.onMeasure(widthMeasureSpec, ViewUtil.getMeasureSpecHeight(mRatio, widthMeasureSpec));
                    break;
                case RATIO_HORIZONTAL:
                    super.onMeasure(ViewUtil.getMeasureSpecWidth(mRatio, heightMeasureSpec), heightMeasureSpec);
                    break;
                default:
                    super.onMeasure(widthMeasureSpec, heightMeasureSpec);
                    break;
            }
        } else {
            super.onMeasure(widthMeasureSpec, heightMeasureSpec);
        }
    }

    @Override
    public void setImageDrawable(@Nullable Drawable drawable) {
        if (mAutoStartGifDrawable == false) {
            stopGifDrawable();
        }
        super.setImageDrawable(drawable);
    }

    //======================================================================
    // Public Methods
    //======================================================================

    public void setOnSuccessListener(GlideLoader.OnSuccessListener listener) {
        mListener.setSuccessListener(listener);
    }

    public void setOnFailListener(GlideLoader.OnFailListener listener) {
        mListener.setFailListener(listener);
    }

    @NonNull
    public TransformationOption getOption() {
        return mOption;
    }

    public void setAutoImageResize(boolean autoImageResize) {
        glideLoader().autoImageResize(autoImageResize);
    }

    public boolean isErrorImageDrawable() {
        return mListener.mFail;
    }

    public void setImageUrl(ImageSize imageSize, String url) {
        glideLoader().override(imageSize.mMaxSize, imageSize.mMaxSize);
        loadImage(url);
    }

    public void setImageUrl(String url) {
        loadImage(url);
    }

    public void setImageUrl(boolean skipMemory, String url) {
        glideLoader().skipMemory(skipMemory);
        loadImage(url);
    }

    public void setBlur(boolean blur) {
        if (blur == true) {
            mTransformationSparseArray.put(KEY_BLUR, BlurBitmapTransformation.create(getContext()));
        } else {
            mTransformationSparseArray.delete(KEY_BLUR);
        }
    }

    public void setBlur(int radius) {
        if (radius > 0) {
            mTransformationSparseArray.put(KEY_BLUR, new BlurBitmapTransformation(radius) {
                @Override
                public Context getContext() {
                    return BaseImageView.this.getContext();
                }
            });
        } else {
            mTransformationSparseArray.delete(KEY_BLUR);
        }
    }

    public void setRoundedBitmapTransformation(RoundedBitmapTransformation transformation) {
        if (transformation != null) {
            mTransformationSparseArray.put(KEY_ROUNDED, transformation);
        } else {
            mTransformationSparseArray.delete(KEY_ROUNDED);
        }
    }

    public void putTransformation(Transformation transformation) {
        if (transformation != null) {
            @SuppressLint("DefaultLocale")
            int key = String.format(FORMAT_TRANSFORMATION, mTransformationSparseArray.size()).hashCode();
            mTransformationSparseArray.put(key, transformation);
        }
    }

    public void setDefaultImage(int defaultImage) {
        glideLoader().defaultImage(defaultImage);
    }

    public void setErrorImage(int errorImage) {
        glideLoader().errorImage(errorImage);
    }

    public void setErrorImage(Drawable errorImage) {
        glideLoader().errorImage(errorImage);
    }

    public void setAnimation(boolean crossFade) {
        glideLoader().crossFade(crossFade);
    }

    public float getRatio() {
        return mRatio;
    }

    public void setRatio(float ratio) {
        if (mRatio != ratio) {
            mRatio = ratio;
            requestLayout();
        }
    }

    public void releaseImage() {
        glideLoader().cancel();
        final Drawable drawable = getDrawable();
        if (drawable != null) {
            drawable.setCallback(null);
        }
        destroyDrawingCache();
        setImageDrawable(null);
    }

    public void onDestroy() {
        try {
            mGlideLoader.cancel();
            mGlideLoader = null;
            releaseImage();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public boolean isUrlTag() {
        if (getTag() instanceof String) {
            String tagUrl = (String) getTag();
            if (StringUtil.startsWith(tagUrl, "http") == true) {
                return true;
            }
        }
        return false;
    }

    public GlideLoader glideLoader() {
        if (mGlideLoader == null) {
            mGlideLoader = new GlideLoader();
        }
        return mGlideLoader;
    }

    public void setAutoStartGifDrawable(boolean autoStartGifDrawable) {
        mAutoStartGifDrawable = autoStartGifDrawable;
    }

    public void startGifDrawable() {
        GifDrawable drawable = findGifDrawable(getDrawable());
        if (drawable != null) {
            drawable.start();
        }
    }

    public void stopGifDrawable() {
        GifDrawable drawable = findGifDrawable(getDrawable());
        if (drawable != null) {
            drawable.stop();
        }
    }

    //======================================================================
    // Private Methods
    //======================================================================

    @Nullable
    public static GifDrawable findGifDrawable(Drawable drawable) {
        if (drawable instanceof TransitionDrawable) {
            TransitionDrawable transitionDrawable = (TransitionDrawable) drawable;
            int length = transitionDrawable.getNumberOfLayers();
            for (int i = 0; i < length; ++i) {
                Drawable child = transitionDrawable.getDrawable(i);
                if (child instanceof GifDrawable) {
                    return (GifDrawable) child;
                }
            }
        } else if (drawable instanceof GifDrawable) {
            return (GifDrawable) drawable;
        }
        return null;
    }

    private boolean isEqualsTag(String url) {
        if (getTag() instanceof String) {
            String tagUrl = (String) getTag();
            return StringUtil.equals(url, tagUrl);
        }
        return false;
    }

    private void loadImage(String url) {
        try {
            boolean equalsTag = isEqualsTag(url);
            boolean drawableUse = getDrawable() != null;

            if (drawableUse == true && equalsTag == false) {
                releaseImage();
            }
            mListener.updateTag(url);
            glideLoader()
                    .crossFade(drawableUse != true || equalsTag != true)
                    .transform(makeTransformations())
                    .successListener(mListener)
                    .failListener(mListener)
                    .load(url, this);
        }catch (Exception e){
            e.printStackTrace();
        }
    }

    private Transformation[] makeTransformations() {
        int size = mTransformationSparseArray.size();

        Transformation[] transformations = new Transformation[size];
        for (int i = 0; i < size; i++) {
            transformations[i] = mTransformationSparseArray.get(mTransformationSparseArray.keyAt(i));
        }
        return transformations;
    }

    //======================================================================
    // ImageSize
    //======================================================================

    public enum ImageSize {
        FULL(1000),
        NORMAL(500);

        int mMaxSize;

        ImageSize(int maxSize) {
            mMaxSize = maxSize;
        }

        public int getMaxSize(@NonNull Resources resources) {
            // TODO: 15. 9. 23. 1280_800(HD) 320dpi 기준으로 세팅
            float scale = 1.0f;
            if (resources.getDisplayMetrics().densityDpi < DisplayMetrics.DENSITY_XHIGH) {
                scale = 0.8f;
            } else if (resources.getDisplayMetrics().densityDpi > DisplayMetrics.DENSITY_560) {
                scale = 1.2f;
            }
            return Math.round(mMaxSize * scale);
        }
    }

    //======================================================================
    // TransformationOption
    //======================================================================

    public final class TransformationOption {

        float mCornerRadius;

        float mBorderWidth;

        @ColorInt
        int mBorderColor;

        @ColorInt
        int mContentColor;

        boolean mOver;

        boolean mRoundedTransformationEnable;

        public TransformationOption() {
        }

        public void init(Context context, AttributeSet attrs) {
            @SuppressLint("Recycle")
            TypedArray a = context.obtainStyledAttributes(attrs, R.styleable.BaseImageView);

            mCornerRadius = a.getDimensionPixelSize(R.styleable.BaseImageView_trans_corner_radius, -1);
            mBorderWidth = a.getDimensionPixelSize(R.styleable.BaseImageView_trans_border_width, -1);
            mBorderColor = a.getColor(R.styleable.BaseImageView_trans_border_color, Color.TRANSPARENT);
            mContentColor = a.getColor(R.styleable.BaseImageView_trans_content_color, Color.TRANSPARENT);
            mOver = a.getBoolean(R.styleable.BaseImageView_trans_over, false);
            setRoundedTransformationEnable(a.getBoolean(R.styleable.BaseImageView_trans_rounded_enable, false));
        }

        public boolean isRoundedTransformationEnable() {
            return mRoundedTransformationEnable;
        }

        public void setRoundedTransformationEnable(boolean roundedTransformationEnable) {
            mRoundedTransformationEnable = roundedTransformationEnable;

            if (mRoundedTransformationEnable == true) {
                setRoundedBitmapTransformation(new RoundedBitmapTransformation.Builder()
                        .defaultRadius(mCornerRadius)
                        .borderWidth(mBorderWidth)
                        .borderColor(mBorderColor)
                        .contentBackgroundColor(mContentColor)
                        .over(mOver)
                        .build(getContext()));
            } else {
                setRoundedBitmapTransformation(null);
            }
        }

        public float getCornerRadius() {
            return mCornerRadius;
        }

        public void setCornerRadius(float cornerRadius) {
            mCornerRadius = cornerRadius;
        }

        public float getBorderWidth() {
            return mBorderWidth;
        }

        public void setBorderWidth(float borderWidth) {
            mBorderWidth = borderWidth;
        }

        public int getBorderColor() {
            return mBorderColor;
        }

        public void setBorderColor(int borderColor) {
            mBorderColor = borderColor;
        }

        public int getContentColor() {
            return mContentColor;
        }

        public void setContentColor(int contentColor) {
            mContentColor = contentColor;
        }

        public boolean isOver() {
            return mOver;
        }

        public void setOver(boolean over) {
            mOver = over;
        }
    }

    //======================================================================
    // RequestListener
    //======================================================================

    final class RequestListenerWrap implements GlideLoader.OnSuccessListener, GlideLoader.OnFailListener {

        String mTag;

        boolean mFail;

        boolean mSuccess;

        GlideLoader.OnSuccessListener mSuccessListener;

        GlideLoader.OnFailListener mFailListener;

        public void setSuccessListener(GlideLoader.OnSuccessListener successListener) {
            mSuccessListener = successListener;
        }

        public void setFailListener(GlideLoader.OnFailListener failListener) {
            mFailListener = failListener;
        }

        @Override
        public boolean onLoadFailed(@Nullable GlideException e, Object model, Target<Drawable> target, boolean isFirstResource) {
            mFail = true;
            mSuccess = false;
            release();
            return mFailListener != null && mFailListener.onLoadFailed(e, model, target, isFirstResource);
        }

        @Override
        public boolean onResourceReady(Drawable resource, Object model, Target<Drawable> target, DataSource dataSource, boolean isFirstResource) {
            if (mTag != null) {
                setTag(mTag);
            }
            mFail = true;
            mSuccess = false;
            release();
            return mSuccessListener != null && mSuccessListener.onResourceReady(resource, model, target, dataSource, isFirstResource);
        }

        //======================================================================
        // Methods
        //======================================================================

        void updateTag(String tag){
            mTag = tag;
        }

        //======================================================================
        // Private Methods
        //======================================================================

        private void release(){
            mTag = null;
        }
    }
}
