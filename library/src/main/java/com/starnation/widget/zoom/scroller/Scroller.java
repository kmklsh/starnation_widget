package com.starnation.widget.zoom.scroller;

import android.content.Context;
import android.os.Build.VERSION;
import android.os.Build.VERSION_CODES;

/**
 *  스크롤 처리 base 클래스
 *
 * @author lsh
 * @since 2014-08-12
 */
public abstract class Scroller {

    public static Scroller getScroller(Context context) {
        //진저브레드 버전이 최소 버전이다 이하버전은 지원 안함
        if (VERSION.SDK_INT < VERSION_CODES.ICE_CREAM_SANDWICH) {
            return new GingerScroller(context);
        } else {
            return new IcsScroller(context);
        }
    }

    public abstract boolean computeScrollOffset();

    public abstract void fling(int startX, int startY, int velocityX, int velocityY, int minX, int maxX, int minY,  int maxY, int overX, int overY);

    public abstract void forceFinished(boolean finished);

    public abstract boolean isFinished();

    public abstract int getCurrX();

    public abstract int getCurrY();
}
