package com.starnation.widget.zoom;

import android.view.View;

/*
 * @author vkwofm
 * @since 2018. 5. 4.
 */
public interface OnSingleTapListener {

    boolean onSingleTap(View v);
}
