package com.starnation.widget.zoom;

import android.graphics.Bitmap;
import android.graphics.Matrix;
import android.graphics.RectF;
import android.view.GestureDetector;
import android.view.View;
import android.widget.ImageView;

/**
 * 이미지 확대/축소 기능 공통 API 정의 인터페이스
 *
 * @author lsh 
 * @since 2014-08-12
 */
public interface IZoomImage {
    /**
     * 가장자리 영역 초기값
     */
    public static final int EDGE_NONE = -1;
    /**
     * 가장자리 영역 왼쪽
     */
    public static final int EDGE_LEFT = 0;
    /**
     * 가장자리 영역 오른쪽
     */
    public static final int EDGE_RIGHT = 1;
    /**
     * 가장자리 영역 벋어난경우
     */
    public static final int EDGE_BOTH = 2;

    /**
     * 최대 스케일값 정의
     */
    public static final float DEFAULT_MAX_SCALE = 3.0f;
    /**
     * 최소 스케일값 정의
     */
    public static final float DEFAULT_MIN_SCALE = 1.0f;
    /**
     * 중간 스케일값 정의
     */
    public static final float DEFAULT_MID_SCALE = 1.75f;
    /**
     * 기본 Zoom 이벤트시 지속 시간 정의
     */
    public static final int DEFAULT_ZOOM_DURATION = 200;

    /**
     * ZoomImage 기능 활성 여부
     *
     * @return
     */
    public abstract boolean canZoom();

    /**
     * ZoomImage 기능 활성 여부 세팅
     *
     * @param zoomable
     */
    public abstract void setZoomable(boolean zoomable);

    /**
     * ZoomImage 현재 크기 정보
     *
     * @return
     */
    public abstract RectF getDisplayRect();

    /**
     * Matrix 리턴
     *
     * @return 현재 사용하고 있는 Matrix 리턴
     */
    public abstract Matrix getDisplayMatrix();

    /**
     * Matrix 저장
     *
     * @param matrix
     * @return
     */
    public abstract boolean setDisplayMatrix(Matrix matrix);

    /**
     * 스케일값 Zoom 이벤트 동작시에는 스케일값이 동적으로 변경된다.
     *
     * @return 스케일값
     */
    public abstract float getScale();

    /**
     * ZoomImage LongClick 이벤트 리스너
     *
     * @param listener
     */
    public abstract void setOnLongClickListener(View.OnLongClickListener listener);

    /**
     * 이미지 회전
     *
     * @param rotationDegree 각도
     */
    public abstract void setRotationTo(float rotationDegree);

    /**
     * 이미지 회전
     *
     * @param rotationDegree 각도
     */
    public abstract void setRotationBy(float rotationDegree);

    /**
     * ZoomImage 스케일
     *
     * @param scale 스케일값
     */
    public abstract void setScale(float scale);

    /**
     * ZoomImage 스케일
     *
     * @param scale   스케일값
     * @param animate 에니메이션 적용 여부
     */
    public abstract void setScale(float scale, boolean animate);

    /**
     * ZoomImage 스케일
     *
     * @param scale   스케일값
     * @param focalX  스케일 처리할 x좌표 위치
     * @param focalY  스케일 처리할 y좌표 위치
     * @param animate
     */
    public abstract void setScale(float scale, float focalX, float focalY, boolean animate);

    /**
     * Zoomiamge ScaleType
     *
     * @return setScaleType 에 저장된 ScaleType으로 리턴
     */
    public abstract ImageView.ScaleType getScaleType();

    /**
     * Zoomiamge ScaleType
     *
     * @param scaleType ScaleType 타입에 정의된 타입
     */
    public abstract void setScaleType(ImageView.ScaleType scaleType);

    /**
     * Zoomiamge Bitmap 파일로 리턴
     *
     * @return Bitmap 으로 리턴 그려진 이미지가 없는경우 null로 리턴한다.
     */
    public abstract Bitmap getBitmap();

    /**
     * Zoomiamge Zoom 이벤트시 지속 시간 기본 duration 은 200ms 0.2초 이다.
     *
     * @param milliseconds 시간 milliseconds 단위
     */
    public abstract void setZoomTransitionDuration(int milliseconds);

    /**
     * 이미지 더블탭 이벤트 리스너
     *
     * @param onDoubleTapListener 리스너
     */
    public void setOnDoubleTapListener(GestureDetector.OnDoubleTapListener onDoubleTapListener);
}
