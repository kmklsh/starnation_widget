package com.starnation.widget.zoom;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Matrix;
import android.graphics.Matrix.ScaleToFit;
import android.graphics.RectF;
import android.graphics.drawable.Drawable;
import android.util.Log;
import android.view.GestureDetector;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnLongClickListener;
import android.view.ViewConfiguration;
import android.view.ViewParent;
import android.view.ViewTreeObserver;
import android.view.animation.AccelerateDecelerateInterpolator;
import android.view.animation.Interpolator;
import android.widget.ImageView;
import android.widget.ImageView.ScaleType;

import com.starnation.widget.zoom.gestures.OnGestureListener;
import com.starnation.widget.zoom.gestures.VersionedGestureDetector;
import com.starnation.widget.zoom.scroller.Scroller;

import java.lang.ref.WeakReference;

import static android.view.MotionEvent.ACTION_CANCEL;
import static android.view.MotionEvent.ACTION_DOWN;
import static android.view.MotionEvent.ACTION_UP;

/**
 * 이미지 확대/축소에 필요한 기능
 *
 * @author lsh
 * @since 2014-08-12
 */
public class ZoomImageHellper implements IZoomImage, View.OnTouchListener, OnGestureListener, ViewTreeObserver.OnGlobalLayoutListener {

    private static final String LOG_TAG = "PhotoViewAttacher";

    private static final Interpolator sInterpolator = new AccelerateDecelerateInterpolator();

    /**
     * Drawable 세팅되어 있는지 확인
     * @param imageView
     * @return
     */
    private static boolean hasDrawable(ImageView imageView) {
        return null != imageView && null != imageView.getDrawable();
    }

    /**
     * scaleType MATRIX 타입을 제외한 타입이 정의 되어 있으면 true 리턴 MATRIX 타입으로 되어 있으면 throw 던진다
     * @param scaleType
     * @return
     */
    private static boolean isSupportedScaleType(final ScaleType scaleType) {
        if (null == scaleType) {
            return false;
        }
        switch (scaleType) {
            case MATRIX:
                throw new IllegalArgumentException(scaleType.name() + " is not supported in PhotoView");
            default:
                return true;
        }
    }

    private static void setImageViewScaleTypeMatrix(ImageView imageView) {
        if (null != imageView && !(imageView instanceof IZoomImage)) {
            if (!ScaleType.MATRIX.equals(imageView.getScaleType())) {
                imageView.setScaleType(ScaleType.MATRIX);
            }
        }
    }

    /**
     * OutOfMemory가 나올수 있으니 약한 참조로 선언한다.
     */
    private WeakReference<ImageView> mImageView;
    /**
     * 기본 Gesture 감지
     */
    private GestureDetector mGestureDetector;
    /**
     * Zoom Gesture 감지
     */
    private com.starnation.widget.zoom.gestures.GestureDetector mZoomGestureDetector;

    private final Matrix mBaseMatrix = new Matrix();
    private final Matrix mDrawMatrix = new Matrix();
    private final Matrix mSuppMatrix = new Matrix();

    private final RectF mDisplayRect = new RectF();

    private final float[] mMatrixValues = new float[9];
    /**
     * Long Click 이벤트
     */
    private OnLongClickListener mLongClickListener;

    private OnSingleTapListener mSingleTapListener;

    private GestureDetector.SimpleOnGestureListener mOnCustomGestureListener;

    /**
     * 이미지뷰 위치
     */
    private int mImageViewTop, mImageViewRight, mImageViewBottom, mImageViewLeft;
    /**
     * 이미지 스크롤 동작 Runnable
     */
    private FlingRunnable mCurrentFlingRunnable;

    private int mScrollEdge = EDGE_BOTH;
    /**
     * ZoomImage 기능 활성 여부 기본은 true
     */
    private boolean mZoomEnabled = true;
    /**
     * ZoomImage 기본 스케일 타입 FIT_CENTER
     */
    private ScaleType mScaleType = ScaleType.FIT_CENTER;
    /**
     * 기본 Zoom Animation 속도
     */
    private int mZoomDuration = DEFAULT_ZOOM_DURATION;
    /**
     * 기본 최소,최대 스케일 별도의 API로 공개할 필요성이 없기 때문에 내부에서 처리한다.
     */
    private final float mMinScale = DEFAULT_MIN_SCALE, mMaxScale = DEFAULT_MAX_SCALE;
    /**
     * 이미지 가장자리 영역 체크 여부 기본은 true 별도의 API로 공개할 필요성이 없기 때문에 내부에서 처리 한다.
     */
    private final boolean mAllowParentInterceptOnEdge = true;

    private Runnable mSingleTapRunnable;

    public ZoomImageHellper(ImageView imageView) {
        mImageView = new WeakReference<ImageView>(imageView);
        imageView.setDrawingCacheEnabled(true);
        imageView.setOnTouchListener(this);
        ViewTreeObserver observer = imageView.getViewTreeObserver();
        if (null != observer){
            observer.addOnGlobalLayoutListener(this);
        }
        setImageViewScaleTypeMatrix(imageView);
        if (imageView.isInEditMode()) {
            return;
        }

        mZoomGestureDetector = VersionedGestureDetector.newInstance(imageView.getContext(), this);
        mGestureDetector = new GestureDetector(imageView.getContext(), new GestureDetector.SimpleOnGestureListener() {
            @Override
            public boolean onSingleTapUp(final MotionEvent e) {
                mSingleTapRunnable = new Runnable() {
                    @Override
                    public void run() {
                        if (null != mSingleTapListener) {
                            mSingleTapListener.onSingleTap(getImageView());
                        }
                        mSingleTapRunnable = null;
                    }
                };
                getImageView().postDelayed(mSingleTapRunnable, ViewConfiguration.getDoubleTapTimeout());

                if (mOnCustomGestureListener != null) {
                    mOnCustomGestureListener.onSingleTapUp(e);
                }
                return super.onSingleTapUp(e);
            }

            @Override
            public void onLongPress(MotionEvent e) {
                if (null != mLongClickListener) {
                    mLongClickListener.onLongClick(getImageView());
                }
                if (mOnCustomGestureListener != null) {
                    mOnCustomGestureListener.onLongPress(e);
                }
            }

            @Override
            public boolean onScroll(MotionEvent e1, MotionEvent e2, float distanceX, float distanceY) {
                if (mOnCustomGestureListener != null) {
                    mOnCustomGestureListener.onScroll(e1, e2, distanceX, distanceY);
                }
                return super.onScroll(e1, e2, distanceX, distanceY);
            }

            @Override
            public boolean onFling(MotionEvent e1, MotionEvent e2, float velocityX, float velocityY) {
                if (mOnCustomGestureListener != null) {
                    mOnCustomGestureListener.onFling(e1, e2, velocityX, velocityY);
                }
                return super.onFling(e1, e2, velocityX, velocityY);
            }

            @Override
            public void onShowPress(MotionEvent e) {
                if (mOnCustomGestureListener != null) {
                    mOnCustomGestureListener.onShowPress(e);
                }
                super.onShowPress(e);
            }

            @Override
            public boolean onDown(MotionEvent e) {
                if (mOnCustomGestureListener != null) {
                    mOnCustomGestureListener.onDown(e);
                }
                return super.onDown(e);
            }

            @Override
            public boolean onDoubleTap(MotionEvent e) {
                if (mOnCustomGestureListener != null) {
                    mOnCustomGestureListener.onDoubleTap(e);
                }
                return super.onDoubleTap(e);
            }

            @Override
            public boolean onDoubleTapEvent(MotionEvent e) {
                if (mOnCustomGestureListener != null) {
                    mOnCustomGestureListener.onDoubleTapEvent(e);
                }
                return super.onDoubleTapEvent(e);
            }

            @Override
            public boolean onSingleTapConfirmed(MotionEvent e) {
                if (mOnCustomGestureListener != null) {
                    mOnCustomGestureListener.onSingleTapConfirmed(e);
                }
                return super.onSingleTapConfirmed(e);
            }
        }
        );
        mGestureDetector.setOnDoubleTapListener(new OnDoubleTapListener(this));
        setZoomable(true);
    }

    @Override
    public void setOnDoubleTapListener(GestureDetector.OnDoubleTapListener newOnDoubleTapListener) {
        if (newOnDoubleTapListener != null)
            this.mGestureDetector.setOnDoubleTapListener(newOnDoubleTapListener);
        else
            this.mGestureDetector.setOnDoubleTapListener(new OnDoubleTapListener(this));
    }

    @Override
    public boolean canZoom() {
        return mZoomEnabled;
    }

    @SuppressWarnings("deprecation")
    public void cleanup() {
        if (null == mImageView) {
            return;
        }
        final ImageView imageView = mImageView.get();
        if (null != imageView) {
            ViewTreeObserver observer = imageView.getViewTreeObserver();
            if (null != observer && observer.isAlive()) {
                observer.removeGlobalOnLayoutListener(this);
            }
            imageView.setOnTouchListener(null);
            cancelFling();
        }
        if (null != mGestureDetector) {
            mGestureDetector.setOnDoubleTapListener(null);
        }
        mImageView = null;
    }

    @Override
    public RectF getDisplayRect() {
        checkMatrixBounds();
        return getDisplayRect(getDrawMatrix());
    }

    @Override
    public boolean setDisplayMatrix(Matrix finalMatrix) {
        if (finalMatrix == null) {
            throw new IllegalArgumentException("Matrix cannot be null");
        }
        ImageView imageView = getImageView();
        if (null == imageView) {
            return false;
        }
        if (null == imageView.getDrawable()) {
            return false;
        }
        mSuppMatrix.set(finalMatrix);
        setImageViewMatrix(getDrawMatrix());
        checkMatrixBounds();
        return true;
    }

    @Override
    public void setRotationTo(float degrees) {
        mSuppMatrix.setRotate(degrees % 360);
        checkAndDisplayMatrix();
    }

    @Override
    public void setRotationBy(float degrees) {
        mSuppMatrix.postRotate(degrees % 360);
        checkAndDisplayMatrix();
    }

    public ImageView getImageView() {
        ImageView imageView = null;
        if (null != mImageView) {
            imageView = mImageView.get();
        }
        if (null == imageView) {
            cleanup();
            Log.i(LOG_TAG, "ImageView no longer exists. You should not use this PhotoViewAttacher any more.");
        }
        return imageView;
    }

    @Override
    public float getScale() {
        return (float) Math.sqrt((float) Math.pow(getValue(mSuppMatrix, Matrix.MSCALE_X), 2) + (float) Math.pow(getValue(mSuppMatrix, Matrix.MSKEW_Y), 2));
    }

    @Override
    public ScaleType getScaleType() {
        return mScaleType;
    }

    @Override
    public void onDrag(float dx, float dy) {
        if (mZoomGestureDetector.isScaling()) {
            return;
        }
        ImageView imageView = getImageView();
        ViewParent parent = imageView.getParent();
        mSuppMatrix.postTranslate(dx, dy);
        checkAndDisplayMatrix();
        if (mAllowParentInterceptOnEdge && !mZoomGestureDetector.isScaling()) {
            if (mScrollEdge == EDGE_BOTH || (mScrollEdge == EDGE_LEFT && dx >= 1f) || (mScrollEdge == EDGE_RIGHT && dx <= -1f)) {
                if (null != parent)
                    parent.requestDisallowInterceptTouchEvent(false);
            }
        } else {
            if (null != parent) {
                parent.requestDisallowInterceptTouchEvent(true);
            }
        }
    }

    @Override
    public void onFling(float startX, float startY, float velocityX, float velocityY) {
        ImageView imageView = getImageView();
        mCurrentFlingRunnable = new FlingRunnable(imageView.getContext());
        mCurrentFlingRunnable.fling(getImageViewWidth(imageView), getImageViewHeight(imageView), (int) velocityX, (int) velocityY);
        imageView.post(mCurrentFlingRunnable);
    }

    @Override
    public void onGlobalLayout() {
        ImageView imageView = getImageView();
        if (null != imageView) {
            if (mZoomEnabled) {
                final int top = imageView.getTop();
                final int right = imageView.getRight();
                final int bottom = imageView.getBottom();
                final int left = imageView.getLeft();

                if (top != mImageViewTop || bottom != mImageViewBottom || left != mImageViewLeft || right != mImageViewRight) {
                    updateBaseMatrix(imageView.getDrawable());
                    mImageViewTop = top;
                    mImageViewRight = right;
                    mImageViewBottom = bottom;
                    mImageViewLeft = left;
                }
            } else {
                updateBaseMatrix(imageView.getDrawable());
            }
        }
    }

    @Override
    public void onScale(float scaleFactor, float focusX, float focusY) {
        if (getScale() < mMaxScale || scaleFactor < 1f) {
            mSuppMatrix.postScale(scaleFactor, scaleFactor, focusX, focusY);
            checkAndDisplayMatrix();
        }
    }


    @Override
    public boolean onTouch(View v, MotionEvent ev) {
        boolean handled = false;
        if (mZoomEnabled && hasDrawable((ImageView) v)) {
            ViewParent parent = v.getParent();
            switch (ev.getAction()) {
                case ACTION_DOWN:
                    if (null != parent){
                        parent.requestDisallowInterceptTouchEvent(true);
                    }else{
                        Log.i(LOG_TAG, "onTouch getParent() returned null");
                    }
                    cancelFling();

                    if (mSingleTapRunnable != null) {
                        getImageView().removeCallbacks(mSingleTapRunnable);
                        mSingleTapRunnable = null;
                    }
                    break;
                case ACTION_CANCEL:
                case ACTION_UP:
                    if (getScale() < mMinScale) {
                        RectF rect = getDisplayRect();
                        if (null != rect) {
                            v.post(new AnimatedZoomRunnable(getScale(), mMinScale, rect.centerX(), rect.centerY()));
                            handled = true;
                        }
                    }
                    break;
            }
            if (isGestureDetector(ev)) {
                handled = true;
            }
        }
        return handled;
    }

    /**
     * Gesture 감지 여부
     * @param ev
     * @return
     */
    private boolean isGestureDetector(MotionEvent ev) {
        boolean zoomGestureDetector = null != mZoomGestureDetector && mZoomGestureDetector.onTouchEvent(ev);
        boolean gestureDetector = null != mGestureDetector && mGestureDetector.onTouchEvent(ev);
        return zoomGestureDetector || gestureDetector;
    }

    @Override
    public void setOnLongClickListener(OnLongClickListener listener) {
        mLongClickListener = listener;
    }

    public void setOnSingleTapListener(OnSingleTapListener singleTapListener) {
        mSingleTapListener = singleTapListener;
    }

    public void setOnCustomGestureListener(GestureDetector.SimpleOnGestureListener onCustomGestureListener) {
        mOnCustomGestureListener = onCustomGestureListener;
    }

    @Override
    public void setScale(float scale) {
        setScale(scale, false);
    }

    @Override
    public void setScale(float scale, boolean animate) {
        ImageView imageView = getImageView();
        if (null != imageView) {
            setScale(scale, (imageView.getRight()) / 2, (imageView.getBottom()) / 2, animate);
        }
    }

    @Override
    public void setScale(float scale, float focalX, float focalY, boolean animate) {
        ImageView imageView = getImageView();
        if (null != imageView) {
            if (animate) {
                imageView.post(new AnimatedZoomRunnable(getScale(), scale, focalX, focalY));
            } else {
                mSuppMatrix.setScale(scale, scale, focalX, focalY);
                checkAndDisplayMatrix();
            }
        }
    }

    @Override
    public void setScaleType(ScaleType scaleType) {
        if (isSupportedScaleType(scaleType) && scaleType != mScaleType) {
            mScaleType = scaleType;
            update();
        }
    }

    @Override
    public void setZoomable(boolean zoomable) {
        mZoomEnabled = zoomable;
        update();
    }

    public void update() {
        ImageView imageView = getImageView();
        if (null != imageView) {
            if (mZoomEnabled) {
                setImageViewScaleTypeMatrix(imageView);
                updateBaseMatrix(imageView.getDrawable());
            } else {
                resetMatrix();
            }
        }
    }

    @Override
    public Matrix getDisplayMatrix() {
        return new Matrix(getDrawMatrix());
    }

    public Matrix getDrawMatrix() {
        mDrawMatrix.set(mBaseMatrix);
        mDrawMatrix.postConcat(mSuppMatrix);
        return mDrawMatrix;
    }

    private void cancelFling() {
        if (null != mCurrentFlingRunnable) {
            mCurrentFlingRunnable.cancelFling();
            mCurrentFlingRunnable = null;
        }
    }

    private void checkAndDisplayMatrix() {
        if (checkMatrixBounds()) {
            setImageViewMatrix(getDrawMatrix());
        }
    }

    private void checkImageViewScaleType() {
        ImageView imageView = getImageView();
        if (null != imageView && !(imageView instanceof IZoomImage)) {
            if (!ScaleType.MATRIX.equals(imageView.getScaleType())) {
                throw new IllegalStateException("The ImageView's ScaleType has been changed since attaching a PhotoViewAttacher");
            }
        }
    }

    private boolean checkMatrixBounds() {
        final ImageView imageView = getImageView();
        if (null == imageView) {
            return false;
        }
        final RectF rect = getDisplayRect(getDrawMatrix());
        if (null == rect) {
            return false;
        }
        final float height = rect.height(), width = rect.width();
        float deltaX = 0, deltaY = 0;
        final int viewHeight = getImageViewHeight(imageView);
        final int viewWidth = getImageViewWidth(imageView);

        if (height <= viewHeight) {
            switch (mScaleType) {
                case FIT_START:
                    deltaY = -rect.top;
                    break;
                case FIT_END:
                    deltaY = viewHeight - height - rect.top;
                    break;
                default:
                    deltaY = (viewHeight - height) / 2 - rect.top;
                    break;
            }
        } else if (rect.top > 0) {
            deltaY = -rect.top;
        } else if (rect.bottom < viewHeight) {
            deltaY = viewHeight - rect.bottom;
        }
        if (width <= viewWidth) {
            switch (mScaleType) {
                case FIT_START:
                    deltaX = -rect.left;
                    break;
                case FIT_END:
                    deltaX = viewWidth - width - rect.left;
                    break;
                default:
                    deltaX = (viewWidth - width) / 2 - rect.left;
                    break;
            }
            mScrollEdge = EDGE_BOTH;
        } else if (rect.left > 0) {
            mScrollEdge = EDGE_LEFT;
            deltaX = -rect.left;
        } else if (rect.right < viewWidth) {
            deltaX = viewWidth - rect.right;
            mScrollEdge = EDGE_RIGHT;
        } else {
            mScrollEdge = EDGE_NONE;
        }
        mSuppMatrix.postTranslate(deltaX, deltaY);
        return true;
    }

    private RectF getDisplayRect(Matrix matrix) {
        ImageView imageView = getImageView();
        if (null != imageView) {
            Drawable d = imageView.getDrawable();
            if (null != d) {
                mDisplayRect.set(0, 0, d.getIntrinsicWidth(), d.getIntrinsicHeight());
                matrix.mapRect(mDisplayRect);
                return mDisplayRect;
            }
        }
        return null;
    }

    @Override
    public Bitmap getBitmap() {
        ImageView imageView = getImageView();
        return imageView == null ? null : imageView.getDrawingCache();
    }

    @Override
    public void setZoomTransitionDuration(int milliseconds) {
        if (milliseconds < 0)
            milliseconds = DEFAULT_ZOOM_DURATION;
        this.mZoomDuration = milliseconds;
    }

    private float getValue(Matrix matrix, int whichValue) {
        matrix.getValues(mMatrixValues);
        return mMatrixValues[whichValue];
    }

    private void resetMatrix() {
        mSuppMatrix.reset();
        setImageViewMatrix(getDrawMatrix());
        checkMatrixBounds();
    }

    private void setImageViewMatrix(Matrix matrix) {
        ImageView imageView = getImageView();
        if (null != imageView) {
            checkImageViewScaleType();
            imageView.setImageMatrix(matrix);
        }
    }

    private void updateBaseMatrix(Drawable drawable) {
        ImageView imageView = getImageView();
        if (null == imageView || null == drawable) {
            return;
        }
        final float viewWidth = getImageViewWidth(imageView);
        final float viewHeight = getImageViewHeight(imageView);
        final int drawableWidth = drawable.getIntrinsicWidth();
        final int drawableHeight = drawable.getIntrinsicHeight();
        final float widthScale = viewWidth / drawableWidth;
        final float heightScale = viewHeight / drawableHeight;

        mBaseMatrix.reset();
        if (mScaleType == ScaleType.CENTER) {
            mBaseMatrix.postTranslate((viewWidth - drawableWidth) / 2F, (viewHeight - drawableHeight) / 2F);
        } else if (mScaleType == ScaleType.CENTER_CROP) {
            float scale = Math.max(widthScale, heightScale);
            mBaseMatrix.postScale(scale, scale);
            mBaseMatrix.postTranslate((viewWidth - drawableWidth * scale) / 2F, (viewHeight - drawableHeight * scale) / 2F);
        } else if (mScaleType == ScaleType.CENTER_INSIDE) {
            float scale = Math.min(1.0f, Math.min(widthScale, heightScale));
            mBaseMatrix.postScale(scale, scale);
            mBaseMatrix.postTranslate((viewWidth - drawableWidth * scale) / 2F, (viewHeight - drawableHeight * scale) / 2F);
        } else {
            RectF mTempSrc = new RectF(0, 0, drawableWidth, drawableHeight);
            RectF mTempDst = new RectF(0, 0, viewWidth, viewHeight);
            switch (mScaleType) {
                case FIT_CENTER:
                    mBaseMatrix.setRectToRect(mTempSrc, mTempDst, ScaleToFit.CENTER);
                    break;
                case FIT_START:
                    mBaseMatrix.setRectToRect(mTempSrc, mTempDst, ScaleToFit.START);
                    break;
                case FIT_END:
                    mBaseMatrix.setRectToRect(mTempSrc, mTempDst, ScaleToFit.END);
                    break;
                case FIT_XY:
                    mBaseMatrix.setRectToRect(mTempSrc, mTempDst, ScaleToFit.FILL);
                    break;
                default:
                    break;
            }
        }
        resetMatrix();
    }

    private int getImageViewWidth(ImageView imageView) {
        if (null == imageView) {
            return 0;
        }
        return imageView.getWidth() - imageView.getPaddingLeft() - imageView.getPaddingRight();
    }

    private int getImageViewHeight(ImageView imageView) {
        if (null == imageView) {
            return 0;
        }
        return imageView.getHeight() - imageView.getPaddingTop() - imageView.getPaddingBottom();
    }

    /**
     * 이미지 스케일시 Zoom in & out 속도 처리 Runnable
     */
    private class AnimatedZoomRunnable implements Runnable {

        private final float mFocalX, mFocalY;
        private final long mStartTime;
        private final double mZoomStart, mZoomEnd;

        public AnimatedZoomRunnable(final float currentZoom, final float targetZoom, final float focalX, final float focalY) {
            mFocalX = focalX;
            mFocalY = focalY;
            mStartTime = System.currentTimeMillis();
            mZoomStart = currentZoom;
            mZoomEnd = targetZoom;
        }

        @Override
        public void run() {
            ImageView imageView = getImageView();
            if (imageView == null) {
                return;
            }
            float t = interpolate();
            float scale = (float) (mZoomStart + t * (mZoomEnd - mZoomStart));
            float deltaScale = (float) (scale / getScale());

            mSuppMatrix.postScale(deltaScale, deltaScale, mFocalX, mFocalY);
            checkAndDisplayMatrix();
            if (t < 1f) {
                Compat.postOnAnimation(imageView, this);
            }
        }

        private float interpolate() {
            float t = 1f * (System.currentTimeMillis() - mStartTime) / mZoomDuration;
            t = Math.min(1f, t);
            t = sInterpolator.getInterpolation(t);
            return t;
        }
    }

    /**
     * 이미지 스크롤 동작 Runnable
     */
    private class FlingRunnable implements Runnable {

        private final Scroller mScroller;

        private int mCurrentX, mCurrentY;

        public FlingRunnable(Context context) {
            mScroller = Scroller.getScroller(context);
        }

        public void cancelFling() {
            mScroller.forceFinished(true);
        }

        public void fling(int viewWidth, int viewHeight, int velocityX, int velocityY) {
            final RectF rect = getDisplayRect();
            if (null == rect) {
                return;
            }
            final int startX = Math.round(-rect.left);
            final int startY = Math.round(-rect.top);
            final int minX, maxX, minY, maxY;

            if (viewWidth < rect.width()) {
                minX = 0;
                maxX = Math.round(rect.width() - viewWidth);
            } else {
                minX = maxX = startX;
            }
            if (viewHeight < rect.height()) {
                minY = 0;
                maxY = Math.round(rect.height() - viewHeight);
            } else {
                minY = maxY = startY;
            }
            mCurrentX = startX;
            mCurrentY = startY;
            if (startX != maxX || startY != maxY) {
                mScroller.fling(startX, startY, velocityX, velocityY, minX, maxX, minY, maxY, 0, 0);
            }
        }

        @Override
        public void run() {
            if (mScroller.isFinished()) {
                return;
            }
            ImageView imageView = getImageView();
            if (null != imageView && mScroller.computeScrollOffset()) {
                final int newX = mScroller.getCurrX();
                final int newY = mScroller.getCurrY();

                mSuppMatrix.postTranslate(mCurrentX - newX, mCurrentY - newY);
                mCurrentX = newX;
                mCurrentY = newY;
                setImageViewMatrix(getDrawMatrix());
                Compat.postOnAnimation(imageView, this);
            }
        }
    }
}
