package com.starnation.widget.zoom.gestures;

/**
 * 터치 이벤트 동작에 대한 인터페이스 리스너
 *
 * @author lsh
 * @since 2014-08-12
 */
public interface OnGestureListener {

    /**
     * 드레그 시작시 이벤트
     *
     * @param x
     * @param y
     */
    public abstract void onDrag(float x, float y);

    /**
     * 드래그중 일경우 이벤트
     *
     * @param startX
     * @param startY
     * @param velocityX
     * @param velocityY
     */
    public abstract void onFling(float startX, float startY, float velocityX, float velocityY);

    /**
     * 줌 인 줌아웃 이벤트
     *
     * @param scaleFactor
     * @param focusX
     * @param focusY
     */
    public abstract void onScale(float scaleFactor, float focusX, float focusY);
}
