package com.starnation.widget.zoom.gestures;

import android.content.Context;

/**
 * OS 버전별 제스처 처리 현재는 최소 버전이 진저브레드이다 상위 버전에 대한 예외처리 없음
 *
 * @author lsh
 * @since 2014-08-12
 */
public final class VersionedGestureDetector {

    /**
     * GestureDetector 사용 OS 버전에 맞게 생성 현재는 최소 버전이 진저브레드 이고 상위 버전에 대한 예외 처리 없음
     * @param context
     * @param listener
     * @return
     */
    public static GestureDetector newInstance(Context context, OnGestureListener listener) {
        GestureDetector detector = new GestureDetector(context);
        detector.setOnGestureListener(listener);
        return detector;
    }
}
