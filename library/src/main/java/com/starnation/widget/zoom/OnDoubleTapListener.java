package com.starnation.widget.zoom;

import android.view.GestureDetector;
import android.view.MotionEvent;

/**
 * 제스처 이벤트 탭 동작에 대한 인터페이스
 *
 * @author lsh
 * @since 2014-08-12
 */
public class OnDoubleTapListener implements GestureDetector.OnDoubleTapListener {

    private ZoomImageHellper mZoomImageHellper;

    public OnDoubleTapListener(ZoomImageHellper zoomImageHellper) {
        setZoomImageHellper(zoomImageHellper);
    }

    public void setZoomImageHellper(ZoomImageHellper newZoomImageHellper) {
        this.mZoomImageHellper = newZoomImageHellper;
    }

    @Override
    public boolean onSingleTapConfirmed(MotionEvent e) {
        if (this.mZoomImageHellper == null)
            return false;
        //SingleTap 이벤트시 처리 현재는 필요가 없서서 기능 삭제 <- 2014-08-13
        return false;
    }

    @Override
    public boolean onDoubleTap(MotionEvent ev) {
        if (mZoomImageHellper == null)
            return false;
        try {
            float scale = mZoomImageHellper.getScale();
            float x = ev.getX();
            float y = ev.getY();

            if (scale < IZoomImage.DEFAULT_MID_SCALE) {
                mZoomImageHellper.setScale(IZoomImage.DEFAULT_MID_SCALE, x, y, true);
            } else if (scale >= IZoomImage.DEFAULT_MID_SCALE && scale < IZoomImage.DEFAULT_MAX_SCALE) {
                mZoomImageHellper.setScale(IZoomImage.DEFAULT_MAX_SCALE, x, y, true);
            } else {
                mZoomImageHellper.setScale(IZoomImage.DEFAULT_MIN_SCALE, x, y, true);
            }
        } catch (ArrayIndexOutOfBoundsException e) {
        }
        return true;
    }

    @Override
    public boolean onDoubleTapEvent(MotionEvent e) {
        return false;
    }
}
