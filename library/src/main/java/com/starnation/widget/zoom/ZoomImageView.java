package com.starnation.widget.zoom;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Matrix;
import android.graphics.RectF;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.support.v7.widget.AppCompatImageView;
import android.util.AttributeSet;
import android.view.GestureDetector;

/**
 * 이미지 확대/축소 ImageView
 *
 * @author lsh
 * @since 2014-08-12
 */
public class ZoomImageView extends AppCompatImageView implements IZoomImage {

    private final ZoomImageHellper mZoomImageHellper;

    private ScaleType mPendingScaleType;

    public ZoomImageView(Context context) {
        this(context, null);
    }

    public ZoomImageView(Context context, AttributeSet attr) {
        this(context, attr, 0);
    }

    public ZoomImageView(Context context, AttributeSet attr, int defStyle) {
        super(context, attr, defStyle);
        super.setScaleType(ScaleType.MATRIX);
        mZoomImageHellper = new ZoomImageHellper(this);

        if (null != mPendingScaleType) {
            setScaleType(mPendingScaleType);
            mPendingScaleType = null;
        }
    }

    @Override
    public void setRotationTo(float rotationDegree) {
        mZoomImageHellper.setRotationTo(rotationDegree);
    }

    @Override
    public void setRotationBy(float rotationDegree) {
        mZoomImageHellper.setRotationBy(rotationDegree);
    }

    @Override
    public boolean canZoom() {
        return mZoomImageHellper.canZoom();
    }

    @Override
    public RectF getDisplayRect() {
        return mZoomImageHellper.getDisplayRect();
    }

    @Override
    public Matrix getDisplayMatrix() {
        return mZoomImageHellper.getDrawMatrix();
    }

    @Override
    public boolean setDisplayMatrix(Matrix finalRectangle) {
        return mZoomImageHellper.setDisplayMatrix(finalRectangle);
    }

    @Override
    public float getScale() {
        return mZoomImageHellper.getScale();
    }

    @Override
    public ScaleType getScaleType() {
        return mZoomImageHellper.getScaleType();
    }

    @Override
    public void setImageDrawable(Drawable drawable) {
        super.setImageDrawable(drawable);
        if (null != mZoomImageHellper) {
            mZoomImageHellper.update();
        }
    }

    @Override
    public void setImageResource(int resId) {
        super.setImageResource(resId);
        if (null != mZoomImageHellper) {
            mZoomImageHellper.update();
        }
    }

    @Override
    public void setImageURI(Uri uri) {
        super.setImageURI(uri);
        if (null != mZoomImageHellper) {
            mZoomImageHellper.update();
        }
    }

    @Override
    public void setImageBitmap(Bitmap bm) {
        super.setImageBitmap(bm);
        if (null != mZoomImageHellper) {
            mZoomImageHellper.update();
        }
    }

    @Override
    public void setScale(float scale) {
        mZoomImageHellper.setScale(scale);
    }

    @Override
    public void setScale(float scale, boolean animate) {
        mZoomImageHellper.setScale(scale, animate);
    }

    @Override
    public void setScale(float scale, float focalX, float focalY, boolean animate) {
        mZoomImageHellper.setScale(scale, focalX, focalY, animate);
    }

    @Override
    public void setScaleType(ScaleType scaleType) {
        if (null != mZoomImageHellper) {
            mZoomImageHellper.setScaleType(scaleType);
        } else {
            mPendingScaleType = scaleType;
        }
    }

    @Override
    public void setZoomable(boolean zoomable) {
        mZoomImageHellper.setZoomable(zoomable);
    }

    @Override
    public Bitmap getBitmap() {
        return mZoomImageHellper.getBitmap();
    }

    @Override
    public void setZoomTransitionDuration(int milliseconds) {
        mZoomImageHellper.setZoomTransitionDuration(milliseconds);
    }

    @Override
    public void setOnDoubleTapListener(GestureDetector.OnDoubleTapListener newOnDoubleTapListener) {
        mZoomImageHellper.setOnDoubleTapListener(newOnDoubleTapListener);
    }

    public void setOnLongClickListener(OnLongClickListener listener) {
        mZoomImageHellper.setOnLongClickListener(listener);
    }

    public void setOnSingleTapListener(OnSingleTapListener singleTapListener) {
        mZoomImageHellper.setOnSingleTapListener(singleTapListener);
    }

    public void setOnCustomGestureListener(GestureDetector.SimpleOnGestureListener customGestureListener) {
        mZoomImageHellper.setOnCustomGestureListener(customGestureListener);
    }

    @Override
    protected void onDetachedFromWindow() {
        mZoomImageHellper.cleanup();
        super.onDetachedFromWindow();
    }
}