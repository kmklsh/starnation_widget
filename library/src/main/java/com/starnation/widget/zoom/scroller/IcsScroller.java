package com.starnation.widget.zoom.scroller;

import android.annotation.TargetApi;
import android.content.Context;
import android.os.Build;

/**
 * ICS 스크롤 처리
 *
 * @author lsh
 * @since 2014-08-12
 */
@TargetApi(Build.VERSION_CODES.ICE_CREAM_SANDWICH)
public class IcsScroller extends GingerScroller {

    public IcsScroller(Context context) {
        super(context);
    }

    @Override
    public boolean computeScrollOffset() {
        return mScroller.computeScrollOffset();
    }
}
