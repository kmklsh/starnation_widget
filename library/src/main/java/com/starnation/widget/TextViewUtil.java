package com.starnation.widget;

import android.content.Context;
import android.graphics.drawable.Drawable;
import android.graphics.drawable.StateListDrawable;
import android.os.Build;
import android.support.annotation.NonNull;
import android.support.v4.content.ContextCompat;
import android.text.Layout;
import android.text.StaticLayout;
import android.widget.TextView;

import java.util.regex.Pattern;

/*
 * @author lsh
 * @since 15. 2. 16.
*/
public class TextViewUtil {

    public static Drawable getDrawableLeft(TextView view) {
        final Drawable[] drawables = view.getCompoundDrawables();
        return drawables[0];
    }

    public static void setDrawableLeft(TextView view, int leftDrawableRes) {
        final Drawable[] drawables = view.getCompoundDrawables();
        final Drawable left = ContextCompat.getDrawable(view.getContext(), leftDrawableRes);
        final Drawable top = drawables[1];
        final Drawable right = drawables[2];
        final Drawable bottom = drawables[3];

        view.setCompoundDrawablesWithIntrinsicBounds(left, top, right, bottom);
        view.setCompoundDrawables(left, top, right, bottom);
    }

    public static void setDrawableLeft(TextView view, Drawable leftDrawableRes) {
        final Drawable[] drawables = view.getCompoundDrawables();

        final Drawable top = drawables[1];
        final Drawable right = drawables[2];
        final Drawable bottom = drawables[3];

        view.setCompoundDrawablesWithIntrinsicBounds(leftDrawableRes, top, right, bottom);
        view.setCompoundDrawables(leftDrawableRes, top, right, bottom);
    }

    public static void setDrawableRight(TextView view, Drawable rightDrawableRes) {
        final Drawable[] drawables = view.getCompoundDrawables();

        final Drawable left = drawables[0];
        final Drawable top = drawables[1];
        final Drawable bottom = drawables[3];

        view.setCompoundDrawablesWithIntrinsicBounds(left, top, rightDrawableRes, bottom);
        view.setCompoundDrawables(left, top, rightDrawableRes, bottom);
    }

    public static void setDrawableRight(TextView view, int rightDrawableRes) {
        final Drawable[] drawables = view.getCompoundDrawables();

        final Drawable left = drawables[0];
        final Drawable top = drawables[1];
        final Drawable right = ContextCompat.getDrawable(view.getContext(), rightDrawableRes);
        final Drawable bottom = drawables[3];

        view.setCompoundDrawablesWithIntrinsicBounds(left, top, right, bottom);
        view.setCompoundDrawables(left, top, right, bottom);
    }

    public static void setDrawableTop(TextView view, int topDrawableRes) {
        setDrawableTop(view, ContextCompat.getDrawable(view.getContext(), topDrawableRes));
    }

    public static void setDrawableTop(TextView view, Drawable topDrawableRes) {
        final Drawable[] drawables = view.getCompoundDrawables();

        final Drawable left = drawables[0];
        final Drawable right = drawables[2];
        final Drawable bottom = drawables[3];

        view.setCompoundDrawablesWithIntrinsicBounds(left, topDrawableRes, right, bottom);
        view.setCompoundDrawables(left, topDrawableRes, right, bottom);
    }

    public static void setDrawableBottom(Context context, TextView view, int bottomDrawableRes) {
        final Drawable[] drawables = view.getCompoundDrawables();

        final Drawable left = drawables[0];
        final Drawable top = drawables[1];
        final Drawable right = drawables[2];
        final Drawable bottom = ContextCompat.getDrawable(context, bottomDrawableRes);

        view.setCompoundDrawablesWithIntrinsicBounds(left, top, right, bottom);
        view.setCompoundDrawables(left, top, right, bottom);
    }

    public static void setDrawable(TextView view, int leftDrawableRes, int topDrawableRes, int rightDrawableRes, int bottomDrawableRes) {
        final Context context = view.getContext();
        final Drawable left = ContextCompat.getDrawable(context, leftDrawableRes);
        final Drawable right = ContextCompat.getDrawable(context, rightDrawableRes);
        final Drawable top = ContextCompat.getDrawable(context, topDrawableRes);
        final Drawable bottom = ContextCompat.getDrawable(context, bottomDrawableRes);

        view.setCompoundDrawablesWithIntrinsicBounds(left, top, right, bottom);
        view.setCompoundDrawables(left, top, right, bottom);
    }

    public static void updateCompoundDrawablesState(TextView view) {
        Drawable[] drawables = view.getCompoundDrawables();
        for (Drawable d : drawables) {
            if (d instanceof StateListDrawable) {
                d.setState(view.getDrawableState());
            }
        }
    }

    public static void setDrawable(TextView view, Drawable left, Drawable top, Drawable right, Drawable bottom) {
        view.setCompoundDrawablesWithIntrinsicBounds(left, top, right, bottom);
        view.setCompoundDrawables(left, top, right, bottom);
    }

    public static String makeTextBaseOnMaxLine(@NonNull TextView textView, int maxLine, String fullText, String ellipsis) {
        Pattern pattern = Pattern.compile("[\\.,…;\\:\\s]*$", Pattern.DOTALL);
        String workingText = "";
        Layout layout = createStaticLayout(textView, fullText);
        if (layout.getLineCount() > maxLine) {
            workingText = fullText.substring(0, layout.getLineEnd(maxLine - 1)).trim();
            while (createStaticLayout(textView, workingText + ellipsis).getLineCount() > maxLine) {
                int lastSpace = workingText.lastIndexOf(' ');
                if (lastSpace == -1) {
                    break;
                }
                workingText = workingText.substring(0, lastSpace);
            }
            workingText = pattern.matcher(workingText).replaceFirst("");

            int fullLen = workingText.length() - ellipsis.length();

            if (fullLen > ellipsis.length()) {
                workingText = workingText.substring(0, fullLen) + ellipsis;
            } else {
                workingText = workingText + ellipsis;
            }
            return workingText;
        }
        return fullText;
    }

    public static StaticLayout createStaticLayout(@NonNull TextView textView, String text) {
        return new StaticLayout(text, textView.getPaint(),
                textView.getMeasuredWidth() - textView.getPaddingLeft() - textView.getPaddingRight(),
                Layout.Alignment.ALIGN_NORMAL,
                getLineSpacingMultiplier(textView),
                0.0f,
                false);
    }

    public static float getLineSpacingMultiplier(@NonNull TextView textView) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN) {
            return textView.getLineSpacingMultiplier();
        } else {
            return 1.0f;
        }
    }
}
