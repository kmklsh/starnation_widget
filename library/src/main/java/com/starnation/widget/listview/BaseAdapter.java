package com.starnation.widget.listview;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;


/*
 * @author lsh
 * @since 14. 12. 8.
*/
public abstract class BaseAdapter extends android.widget.BaseAdapter {

    //======================================================================
    // Abstract Methods
    //======================================================================

    public abstract Context getContext();

    public abstract BaseListViewHolder getBaseViewHolder();


    //======================================================================
    // Override Methods
    //======================================================================

    @Override
    public long getItemId(int position) {
        return position;
    }


    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        return createView(position, convertView, parent);
    }

    @Override
    public View getDropDownView(int position, View convertView, ViewGroup parent) {
        return createViewDropDownView(position, convertView, parent);
    }

    //======================================================================
    // Public Methods
    //======================================================================

    public BaseListViewHolder getDropDownViewHolder() {
        return null;
    }

    //======================================================================
    // Private Methods
    //======================================================================

    @SuppressWarnings("unchecked")
    private View createView(int position, View convertView, ViewGroup parent) {
        if (getContext() == null) {
            return null;
        }

        BaseListViewHolder baseListViewHolder;

        if (convertView == null || !(convertView.getTag() instanceof BaseListViewHolder)) {
            baseListViewHolder = getBaseViewHolder();
            convertView = LayoutInflater.from(getContext()).inflate(baseListViewHolder.getViewId(), parent, false);
            baseListViewHolder.init(convertView);
            convertView.setTag(baseListViewHolder);
        } else {
            baseListViewHolder = (BaseListViewHolder) convertView.getTag();
        }

        final Object item = getItem(position);
        baseListViewHolder.refresh(position, item);
        return convertView;
    }

    @SuppressWarnings("unchecked")
    private View createViewDropDownView(int position, View convertView, ViewGroup parent) {
        if (getContext() == null) {
            return null;
        }

        BaseListViewHolder baseListViewHolder;

        if (convertView == null || !(convertView.getTag() instanceof BaseListViewHolder)) {
            baseListViewHolder = getDropDownViewHolder();
            if (baseListViewHolder == null) {
                return createView(position, convertView, parent);
            }

            convertView = LayoutInflater.from(getContext()).inflate(baseListViewHolder.getViewId(), parent, false);
            baseListViewHolder.init(convertView);
            convertView.setTag(baseListViewHolder);
        } else {
            baseListViewHolder = (BaseListViewHolder) convertView.getTag();
        }

        final Object item = getItem(position);
        baseListViewHolder.refresh(position, item);
        return convertView;
    }
}
