package com.starnation.widget.listview;

import android.view.View;

/*
 * @author lsh
 * @since 14. 12. 13.
*/
public abstract class BaseListViewHolder<T> {

    public abstract int getViewId();

    public abstract void init(View view);

    public abstract void refresh(int position, T t);
}
