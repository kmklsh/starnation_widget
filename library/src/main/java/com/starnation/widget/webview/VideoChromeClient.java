package com.starnation.widget.webview;

import android.app.Activity;
import android.content.Context;
import android.content.pm.ActivityInfo;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;

/*
 * @author lsh
 * @since 15. 1. 27.
*/
public abstract class VideoChromeClient extends WebChromeClient {

    //======================================================================
    // Variables
    //======================================================================

    private View mView;

    private int mOriginalOrientation;

    private FullScreenLayout mFullscreenContainer;

    private WebChromeClient.CustomViewCallback mCustomViewCollback;

    private boolean mFullScreenLayout;

    //======================================================================
    // Override Methods
    //======================================================================

    @Override
    public void onShowCustomView(View view, CustomViewCallback callback) {
        super.onShowCustomView(view, callback);
        if (mView != null) {
            callback.onCustomViewHidden();
            return;
        }

        Activity activity = ((Activity) getContext());

        mOriginalOrientation = activity.getRequestedOrientation();

        FrameLayout decor = (FrameLayout) activity.getWindow().getDecorView();

        mFullscreenContainer = new FullScreenLayout(activity);
        mFullscreenContainer.addView(view, ViewGroup.LayoutParams.MATCH_PARENT);
        decor.addView(mFullscreenContainer, ViewGroup.LayoutParams.MATCH_PARENT);
        mView = view;
        mCustomViewCollback = callback;
        activity.setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_LANDSCAPE);
        mFullScreenLayout = true;
    }

    @Override
    public void onHideCustomView() {
        super.onHideCustomView();
        if (mView == null) {
            return;
        }

        Activity activity = ((Activity) getContext());

        FrameLayout decor = (FrameLayout) activity.getWindow().getDecorView();
        decor.removeView(mFullscreenContainer);
        mFullscreenContainer = null;
        mView = null;
        mCustomViewCollback.onCustomViewHidden();
        activity.setRequestedOrientation(mOriginalOrientation);
        mFullScreenLayout = false;
    }

    public boolean isFullScreenLayout() {
        return mFullScreenLayout;
    }

    //======================================================================
    // Layout
    //======================================================================

    public final static class FullScreenLayout extends FrameLayout {

        public FullScreenLayout(Context context) {
            super(context);
            setBackgroundColor(context.getResources().getColor(android.R.color.black));
        }

        @Override
        public boolean onTouchEvent(MotionEvent evt) {
            return true;
        }
    }
}
