package com.starnation.widget.webview;

import android.annotation.SuppressLint;
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.os.Build;
import android.os.Message;
import android.util.AttributeSet;
import android.view.View;
import android.webkit.WebBackForwardList;
import android.webkit.WebHistoryItem;
import android.webkit.WebSettings;
import android.webkit.WebView;

import com.starnation.util.StringUtil;
import com.starnation.widget.WidgetLogger;
import com.starnation.widget.WidgetTag;

/*
 * @author lsh
 * @since 14. 12. 31.
*/
public class BaseAppBrowser extends WebView {

    //======================================================================
    // Constants
    //======================================================================

    private final static String[] HTTP_SUFFIX = {"http://wwww.", "http://m."};

    //======================================================================
    // Variables
    //======================================================================

    private OnClientCallback mOnClientCallback;

    private OnUrlLoadingCallback mOnUrlLoadingCallback;

    private VideoChromeClient mVideoChromeClient;

    //======================================================================
    // Constructor
    //======================================================================

    static {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {
            WebView.setWebContentsDebuggingEnabled(true);
        }
    }

    public BaseAppBrowser(Context context) {
        super(context);
    }

    public BaseAppBrowser(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    //======================================================================
    // Private Methods
    //======================================================================

    @SuppressWarnings("deprecation")
    @SuppressLint("SetJavaScriptEnabled")
    public void init(Type type) {
        switch(type) {
            case NORMAL:
            case CACHE_ENABLE:
                setBackgroundColor(Color.TRANSPARENT);
                getSettings().setJavaScriptEnabled(true);
                // Modify 2015. 10. 15. lsh  새로운 WebView(페이지) 띄우는 처리는 기존 WebView 를 제거 하기 때문에 옵션을 주지 말자!
//              getSettings().setSupportMultipleWindows(true);
                getSettings().setCacheMode(WebSettings.LOAD_NO_CACHE);
                getSettings().setBuiltInZoomControls(false);
                getSettings().setLoadWithOverviewMode(true);
                getSettings().setUseWideViewPort(true);
                setScrollBarStyle(WebView.SCROLLBARS_OUTSIDE_OVERLAY);
                setScrollbarFadingEnabled(false);
                getSettings().setLayoutAlgorithm(WebSettings.LayoutAlgorithm.NORMAL);
                setInitialScale(1);
                clearHistory();
                clearFormData();
                getSettings().setPluginState(WebSettings.PluginState.ON);
                getSettings().setSupportZoom(true);
                getSettings().setJavaScriptCanOpenWindowsAutomatically(true);

                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                    getSettings().setMixedContentMode(WebSettings.MIXED_CONTENT_ALWAYS_ALLOW);
                }
                requestFocus(View.FOCUS_DOWN);
                setWebChromeClient(new WebChromeClient() {
                    @Override
                    public Context getContext() {
                        return BaseAppBrowser.this.getContext();
                    }

                    @Override
                    public boolean onCreateWindow(WebView view, boolean isDialog, boolean isUserGesture, Message resultMsg) {
                        // Modify 2015. 10. 15. lsh getSettings().setSupportMultipleWindows(true); 인 경우 처리
                        WebView newWebView = new WebView(getContext());
                        WebViewTransport transport = (WebViewTransport) resultMsg.obj;
                        transport.setWebView(newWebView);
                        resultMsg.sendToTarget();
                        return true;
                    }

                    @Override
                    public void onReceivedIcon(WebView view, Bitmap icon) {
                        super.onReceivedIcon(view, icon);
                        if (mOnClientCallback != null) {
                            mOnClientCallback.onReceivedIcon(view, icon);
                        }
                    }

                    @Override
                    public void onReceivedTitle(WebView view, String title) {
                        super.onReceivedTitle(view, title);
                        if (mOnClientCallback != null) {
                            mOnClientCallback.onReceivedTitle(view, title);
                        }
                    }

                    @Override
                    public void onProgressChanged(WebView view, int newProgress) {
                        super.onProgressChanged(view, newProgress);
                        if (mOnClientCallback != null) {
                            mOnClientCallback.onProgressChange(view, newProgress);
                        }
                    }
                });
                break;
            case VIDEO:
                getSettings().setJavaScriptEnabled(true);
                getSettings().setPluginState(WebSettings.PluginState.ON);
                getSettings().setSupportMultipleWindows(true);
                mVideoChromeClient = new VideoChromeClient() {
                    @Override
                    public Context getContext() {
                        return BaseAppBrowser.this.getContext();
                    }

                    @Override
                    public void onReceivedIcon(WebView view, Bitmap icon) {
                        super.onReceivedIcon(view, icon);
                        if (mOnClientCallback != null) {
                            mOnClientCallback.onReceivedIcon(view, icon);
                        }
                    }

                    @Override
                    public void onReceivedTitle(WebView view, String title) {
                        super.onReceivedTitle(view, title);
                        if (mOnClientCallback != null) {
                            mOnClientCallback.onReceivedTitle(view, title);
                        }
                    }

                    @Override
                    public void onProgressChanged(WebView view, int newProgress) {
                        super.onProgressChanged(view, newProgress);
                        if (mOnClientCallback != null) {
                            mOnClientCallback.onProgressChange(view, newProgress);
                        }
                    }
                };
                setWebChromeClient(mVideoChromeClient);
                break;
        }
        setWebViewClient(new WebViewClient() {
            @Override
            public boolean shouldOverrideUrlLoading(WebView view, String url) {
                if (mOnUrlLoadingCallback != null) {
                    return mOnUrlLoadingCallback.onShouldOverrideUrlLoading(view, url);
                }
                return super.shouldOverrideUrlLoading(view, url);
            }

            @Override
            public void onPageStarted(WebView view, String url, Bitmap favicon) {
                super.onPageStarted(view, url, favicon);
                if (mOnClientCallback != null) {
                    mOnClientCallback.onPageStarted(view, url, favicon);
                }
            }

            @Override
            public void onPageFinished(WebView view, String url) {
                super.onPageFinished(view, url);
                if (mOnClientCallback != null) {
                    mOnClientCallback.onPageFinished(view, url);
                }
            }
        });
    }

    //======================================================================
    // Override Methods
    //======================================================================

    @Override
    public boolean canGoBack() {
        return super.canGoBack();
    }

    @Override
    public void loadUrl(String url) {
        super.loadUrl(url);
        WidgetLogger.w(WidgetTag.WEB_VIEW, "BaseAppBrowser -> url : " + url);
    }

    //======================================================================
    // Public Methods
    //======================================================================

    public void setOnClientCallback(OnClientCallback onClientCallback) {
        mOnClientCallback = onClientCallback;
    }

    public void setOnUrlLoadingCallback(OnUrlLoadingCallback onUrlLoadingCallback) {
        mOnUrlLoadingCallback = onUrlLoadingCallback;
    }

    public void postLoadUrl(final String url) {
        loadUrl(url);
    }

    public void release() {
        clearHistory();
        clearFormData();
        destroy();
    }

    @SuppressWarnings("unused")
    public boolean onHideCustomView() {
        if (mVideoChromeClient != null) {
            mVideoChromeClient.onHideCustomView();
            return true;
        }
        return false;
    }

    @SuppressWarnings("unused")
    public boolean isVideoFullScreenLayout() {
        return mVideoChromeClient != null && mVideoChromeClient.isFullScreenLayout();
    }

    public boolean canSupportGoBack() {
        goBack();

        final WebBackForwardList backForwardList = copyBackForwardList();
        if (backForwardList != null) {
            WebHistoryItem current = backForwardList.getCurrentItem();
            if (current != null) {
                if (mOnClientCallback != null) {
                    mOnClientCallback.onReceivedTitle(this, current.getTitle());
                }
            }
        }
        return canGoBack();
    }

    //======================================================================
    // Private Methods
    //======================================================================

    @SuppressWarnings({"PointlessBooleanExpression", "deprecation", "unused"})
    @Deprecated
    private static boolean isBaseUrl(String baseUrl, String url) {
        if (StringUtil.isEmpty(baseUrl) == false && StringUtil.isEmpty(url) == false) {
            final String replaceBaseUrl = getUrl(baseUrl);
            final String replaceUrl = getUrl(url);
            return StringUtil.equals(replaceBaseUrl, replaceUrl);
        }
        return false;
    }

    @SuppressWarnings("PointlessBooleanExpression")
    @Deprecated
    private static String getUrl(String url) {
        if (StringUtil.isEmpty(url) == false) {
            for (String suffix : HTTP_SUFFIX) {
                if (url.startsWith(suffix) == true) {
                    String resultUrl = url.replaceAll(suffix, "");
                    WidgetLogger.d(WidgetTag.WEB_VIEW, "resultUrl : " + resultUrl);
                    return resultUrl;
                }
            }
        }
        return "";
    }

    //======================================================================
    // Type
    //======================================================================

    public enum Type {
        NORMAL,
        VIDEO,
        CACHE_ENABLE,
    }

    //======================================================================
    // OnClientCallback
    //======================================================================

    public interface OnClientCallback {

        void onPageStarted(WebView webView, String url, Bitmap favicon);

        void onPageFinished(WebView webView, String url);

        void onProgressChange(WebView webView, int newProgress);

        void onReceivedIcon(WebView webView, Bitmap icon);

        void onReceivedTitle(WebView webView, String title);
    }

    //======================================================================
    // OnClientCallback
    //======================================================================

    public interface OnUrlLoadingCallback {

        boolean onShouldOverrideUrlLoading(WebView view, String url);
    }
}
