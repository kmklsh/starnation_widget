package com.starnation.widget.webview;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.webkit.GeolocationPermissions;
import android.webkit.JsResult;
import android.webkit.WebView;


/*
 * @author lsh
 * @since 15. 1. 27.
*/
public abstract class WebChromeClient extends android.webkit.WebChromeClient {

    //======================================================================
    // Abstract Methods
    //======================================================================

    public abstract Context getContext();

    //======================================================================
    // Override Methods
    //======================================================================

    @Override
    public void onGeolocationPermissionsHidePrompt() {
        super.onGeolocationPermissionsHidePrompt();
    }

    @Override
    public void onGeolocationPermissionsShowPrompt(String origin, GeolocationPermissions.Callback callback) {
        super.onGeolocationPermissionsShowPrompt(origin, callback);
        if (callback != null)
            callback.invoke(origin, true, false);
    }

    @Override
    public boolean onJsAlert(WebView view, String url, String message, final JsResult result) {
        new AlertDialog.Builder(getContext()).setMessage(message).setPositiveButton(android.R.string.ok, new AlertDialog.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                result.confirm();

            }
        }).setCancelable(false).create().show();
        return true;

    }

    public boolean onJsConfirm(WebView view, String url, String message, final JsResult result) {
        new AlertDialog.Builder(getContext()).setMessage(message).setPositiveButton(android.R.string.ok, new AlertDialog.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                result.confirm();
            }
        }).setNegativeButton(android.R.string.cancel, new AlertDialog.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
                result.cancel();
            }
        }).setCancelable(false).create().show();
        return true;
    }


}
