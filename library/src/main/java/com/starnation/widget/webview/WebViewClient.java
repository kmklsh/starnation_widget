package com.starnation.widget.webview;

import android.net.http.SslError;
import android.webkit.SslErrorHandler;
import android.webkit.WebView;

import com.starnation.util.StringUtil;
import com.starnation.widget.WidgetLogger;
import com.starnation.widget.WidgetTag;

/*
 * @author lsh
 * @since 15. 2. 10.
*/
public class WebViewClient extends android.webkit.WebViewClient {

    //======================================================================
    // Constants
    //======================================================================

    private final static String HTTP = "http";

    //======================================================================
    // Override Methods
    //======================================================================

    @Override
    public boolean shouldOverrideUrlLoading(WebView view, String url) {
        WidgetLogger.d(WidgetTag.WEB_VIEW, "shouldOverrideUrlLoading url : " + url);
        if (StringUtil.startsWith(url, HTTP) == true) {
            view.loadUrl(url);
            return false;
        }
        return true;
    }


    @Override
    public void onReceivedError(WebView view, int errorCode, String description, String failingUrl) {
        super.onReceivedError(view, errorCode, description, failingUrl);
        WidgetLogger.d(WidgetTag.WEB_VIEW, "onReceivedError errorCode : " + errorCode + " description : " + description + " failingUrl : " + failingUrl);
        view.loadUrl(failingUrl);
    }

    @Override
    public void onReceivedSslError(WebView view, SslErrorHandler handler, SslError error) {
        super.onReceivedSslError(view, handler, error);
        WidgetLogger.d(WidgetTag.WEB_VIEW, "onReceivedSslError onReceivedSslError : " + error + " handler : " + handler);
        handler.proceed();
    }
}
