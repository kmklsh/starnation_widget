package com.starnation.widget.tutorial;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.support.annotation.DrawableRes;
import android.support.annotation.NonNull;
import android.support.annotation.StringRes;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.RelativeLayout;

import com.starnation.android.view.ViewUtil;
import com.starnation.util.validator.CollectionValidator;
import com.starnation.widget.R;

import java.util.ArrayList;

/*
 * @author lsh
 * @since 15. 7. 27.
*/
public class TutorialView extends RelativeLayout implements View.OnClickListener {

    //======================================================================
    // Variables
    //======================================================================

    private ArrayList<TutorialDrawing> mTutorialDrawingList = new ArrayList<>();

    private Callback mCallback;

    private Button mButton;

    private final int mColor;

    //======================================================================
    // Constructor
    //======================================================================

    public TutorialView(Context context) {
        this(context, null);
    }

    public TutorialView(Context context, AttributeSet attrs) {
        super(context, attrs);

        mButton = (Button) LayoutInflater.from(getContext()).inflate(R.layout.widget_button_tutorial, this, false);
        mButton.setOnClickListener(this);

        LayoutParams params = new LayoutParams(LayoutParams.MATCH_PARENT, LayoutParams.WRAP_CONTENT);

        int margin = getResources().getDimensionPixelSize(R.dimen.commons_default_margin);
        params.setMargins(margin, margin, margin, margin);

        params.addRule(ALIGN_PARENT_BOTTOM);
        addView(mButton, params);

        setButtonText(R.string.commons_ok);

        setOnClickListener(this);

        mColor = getResources().getColor(R.color.tutorial_backgropund);

        ViewUtil.hide(mButton);
    }

    //======================================================================
    // Override Methods
    //======================================================================

    @Override
    public void onClick(View v) {
        process();
    }

    @Override
    protected void dispatchDraw(@NonNull Canvas canvas) {
        if (CollectionValidator.isValid(mTutorialDrawingList) == true) {
            Paint paint = new Paint(Paint.FILTER_BITMAP_FLAG);
            paint.setColor(mColor);
            canvas.drawRect(0, 0, canvas.getWidth(), canvas.getHeight(), paint);
            for (TutorialDrawing tutorialDrawing : mTutorialDrawingList) {
                tutorialDrawing.draw(canvas, paint);
            }
        }
        super.dispatchDraw(canvas);
    }

    //======================================================================
    // Public Methods
    //======================================================================

    public void addTutorial(TutorialDrawing tutorialDrawing) {
        mTutorialDrawingList.add(tutorialDrawing);
        invalidate();
    }

    public void setCallback(Callback callback) {
        mCallback = callback;
    }

    public void setButtonText(@StringRes int text) {
        mButton.setText(text);
    }

    public void setButtonBackground(@DrawableRes int background) {
        mButton.setBackgroundResource(background);
    }

    //======================================================================
    // Private Methods
    //======================================================================

    private void process() {
        if (mCallback != null) {
            mCallback.onHide();
        }
    }

    //======================================================================
    // Callback
    //======================================================================

    public interface Callback {
        void onHide();
    }
}
