package com.starnation.widget.tutorial;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.PointF;
import android.graphics.PorterDuff;
import android.graphics.PorterDuffXfermode;
import android.graphics.Rect;
import android.support.annotation.DrawableRes;
import android.support.annotation.NonNull;
import android.view.View;

/*
 * @author lsh
 * @since 15. 7. 28.
*/
public class TutorialDrawing {

    //======================================================================
    // Variables
    //======================================================================

    private Rect mRect;

    private Bitmap mBuffer;

    private float mCircleScale = 1.0f;

    private boolean mViewCircleDraw = true;

    private View mView;

    private Context mContext;

    @DrawableRes
    private int mRes = -1;

    private PointF mMargin;

    //======================================================================
    // Constructor
    //======================================================================

    public static TutorialDrawing create(View view, boolean viewCircleDraw) {
        TutorialDrawing drawing = new TutorialDrawing(view);
        drawing.setViewCircleDraw(viewCircleDraw);
        return drawing;
    }

    public static TutorialDrawing create(View view, int res, float xMargin, float yMargin, boolean viewCircleDraw) {
        TutorialDrawing drawing = new TutorialDrawing(view);
        drawing.setViewCircleDraw(viewCircleDraw);
        drawing.setRes(res, xMargin, yMargin);
        return drawing;
    }

    public static TutorialDrawing create(View view, int res, float xMargin, float yMargin, boolean viewCircleDraw, float circleScale) {
        TutorialDrawing drawing = new TutorialDrawing(view);
        drawing.setViewCircleDraw(viewCircleDraw);
        drawing.setRes(res, xMargin, yMargin);
        drawing.setCircleScale(circleScale);
        return drawing;
    }

    public static TutorialDrawing create(Context context, Rect rect, int res, float xMargin, float yMargin, boolean viewCircleDraw) {
        TutorialDrawing drawing = new TutorialDrawing(context, rect);
        drawing.setViewCircleDraw(viewCircleDraw);
        drawing.setRes(res, xMargin, yMargin);
        return drawing;
    }

    public TutorialDrawing(View view) {
        mView = view;
        mRect = new Rect();
    }

    public TutorialDrawing(Context context, Rect rect) {
        mContext = context;
        mRect = new Rect();
    }

    //======================================================================
    // Public Methods
    //======================================================================

    public Bitmap decodeResource(@DrawableRes int res) {
        return BitmapFactory.decodeResource(getContext().getResources(), res);
    }

    /**
     * 꼭 호출 해야된다
     *
     * @param canvas
     * @param paint
     */
    public void draw(@NonNull Canvas canvas, @NonNull Paint paint) {
        if (mView != null) {
            mView.getGlobalVisibleRect(mRect);
        }

        float centerX = mRect.exactCenterX();
        float centerY = mRect.exactCenterY();

        if (mViewCircleDraw == true) {
            paint.setAntiAlias(true);
            paint.setXfermode(new PorterDuffXfermode(PorterDuff.Mode.CLEAR));

            float radius = (float) (getHeight() * 0.5);
            canvas.drawCircle(centerX, centerY, radius * mCircleScale, paint);
        }

        if (mRes != -1) {
            Bitmap bitmap = decodeResource(mRes);
            int marginX = calculatorMargin((int) centerX, (int) (mRect.width() * mCircleScale), bitmap.getWidth(), (int) mMargin.x);
            int marginY = calculatorMargin((int) centerY, (int) (mRect.height() * mCircleScale), bitmap.getHeight(), (int) mMargin.y);
            drawBitmap(canvas, bitmap, marginX, marginY);
            try {
                bitmap.recycle();
            } catch (Exception e) {
            }
        }
    }

    public void drawBitmap(@NonNull Canvas canvas, @NonNull Bitmap bitmap, int x, int y) {
        try {
            if (x < 0) x = 0;
            if (y < 0) y = 0;
            if (x > canvas.getWidth() - bitmap.getWidth())
                x = canvas.getWidth() - bitmap.getWidth();
            if (y > canvas.getHeight() - bitmap.getHeight())
                y = canvas.getHeight() - bitmap.getHeight();
            canvas.drawBitmap(bitmap, x, y /*- mStatusBarHeight*/, null);
        } catch (Exception e) {
        }
    }

    public final Context getContext() {
        if (mContext != null) {
            return mContext;
        }
        return mView.getContext();
    }

    public Rect getRect() {
        return mRect;
    }

    public int getLeft() {
        return mRect.left;
    }

    public int getRight() {
        return mRect.right;
    }

    public int getTop() {
        return mRect.top;
    }

    public int getBottom() {
        return mRect.bottom;
    }

    public int getCenterX() {
        return mRect.centerX();
    }

    public int getCenterY() {
        return mRect.centerY();
    }

    public int getWidth() {
        if (mView != null) {
            return mView.getWidth();
        }
        if (mRect.width() > 0) {
            return mRect.width();
        }
        return 0;
    }

    public int getHeight() {
        if (mView != null) {
            return mView.getHeight();
        }
        if (mRect.height() > 0) {
            return mRect.height();
        }
        return 0;
    }

    public final View getView() {
        return mView;
    }

    public Bitmap getBuffer() {
        if (mBuffer == null) {
            mBuffer = Bitmap.createBitmap(getWidth(), getHeight(), Bitmap.Config.ARGB_8888);
        }
        return mBuffer;
    }

    public void setCircleScale(float circleScale) {
        mCircleScale = circleScale;
    }

    public void setViewCircleDraw(boolean viewCircleDraw) {
        mViewCircleDraw = viewCircleDraw;
    }

    public void setRes(int res, float xMargin, float yMargin) {
        mRes = res;
        mMargin = new PointF(xMargin, yMargin);
    }

    public void release() {
        if (mBuffer != null && mBuffer.isRecycled() == false) {
            mBuffer.recycle();
            mBuffer = null;
        }
    }

    //======================================================================
    // Private Methods
    //======================================================================

    @Deprecated
    private boolean isTutorialView() {
        return mView instanceof TutorialView;
    }

    private int calculatorMargin(int position, int viewSize, int bitmapSize, int margin) {
        if (margin == Integer.MIN_VALUE || margin == 0) {
            return position - (bitmapSize / 2);
        } else if (margin < 0) {
            return position - (viewSize / 2) - Math.abs(margin) - bitmapSize;
        } else {
            return position + (viewSize / 2) + Math.abs(margin);
        }
    }
}
