package com.starnation.widget.tutorial;

import android.annotation.SuppressLint;
import android.app.Dialog;
import android.content.Context;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.support.annotation.NonNull;
import android.view.Gravity;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;

import com.starnation.android.content.preference.SharedPreference;

import java.util.ArrayList;
import java.util.List;

/*
 * @author lsh
 * @since 15. 7. 29.
*/
public abstract class Tutorial {

    //======================================================================
    // Variables
    //======================================================================

    private Dialog mDialog;

    private TutorialView.Callback mCallback;

    private List<TutorialDrawing> mTutorialDrawingList = new ArrayList<>();

    private final String mSaveKey;

    private final SharedPreference mPreference;

    //======================================================================
    // Constructor
    //======================================================================

    public Tutorial(String saveKey, SharedPreference preference) {
        mSaveKey = saveKey;
        mPreference = preference;
    }

    //======================================================================
    // Abstract Methods
    //======================================================================

    public abstract TutorialView newTutorialView(@NonNull Context context);

    //======================================================================
    // Public Methods
    //======================================================================

    public boolean isShowing() {
        return mDialog != null && mDialog.isShowing();
    }

    public boolean isShowTutorial() {
        return mPreference.getBoolean(mSaveKey, false) == true;
    }

    public void addTutorialDrawing(TutorialDrawing tutorialDrawing) {
        mTutorialDrawingList.add(tutorialDrawing);
    }

    public void setCallback(TutorialView.Callback callback) {
        mCallback = callback;
    }

    @SuppressLint("RtlHardcoded")
    @SuppressWarnings("ConstantConditions")
    public void show(@NonNull Context context) {
        try {
            if (mSaveKey == null) {
                throw new NullPointerException("saveKey null");
            }
            if (isShowTutorial() == true || mDialog != null) {
                return;
            }

            TutorialView tutorialView = newTutorialView(context);
            for (TutorialDrawing drawing : mTutorialDrawingList) {
                tutorialView.addTutorial(drawing);
            }
            tutorialView.setCallback(newCallback());

            mDialog = new Dialog(tutorialView.getContext(), android.R.style.Theme_Translucent_NoTitleBar_Fullscreen);
            mDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
            mDialog.setContentView(tutorialView);
            mDialog.getWindow().clearFlags(WindowManager.LayoutParams.FLAG_DIM_BEHIND);
            mDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
            WindowManager.LayoutParams params = mDialog.getWindow().getAttributes();
            params.width = ViewGroup.LayoutParams.MATCH_PARENT;
            params.height = ViewGroup.LayoutParams.MATCH_PARENT;
            params.gravity = Gravity.TOP | Gravity.LEFT;
            mDialog.getWindow().setAttributes(params);
            mDialog.setCancelable(true);
            mDialog.show();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void release() {
        for (TutorialDrawing drawing : mTutorialDrawingList) {
            drawing.release();
        }
        mTutorialDrawingList.clear();
        try {
            if (mDialog != null) {
                mDialog.dismiss();
                mDialog = null;
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        mCallback = null;
    }

    //======================================================================
    // Private Methods
    //======================================================================

    private TutorialView.Callback newCallback() {
        return new TutorialView.Callback() {
            @Override
            public void onHide() {
                if (mDialog != null) {
                    mDialog.hide();
                    mDialog = null;
                }
                mPreference.putBoolean(mSaveKey, true).save();
                if (mCallback != null) {
                    mCallback.onHide();
                }
            }
        };
    }
}
