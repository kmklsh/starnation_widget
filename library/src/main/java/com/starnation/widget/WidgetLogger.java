package com.starnation.widget;

import com.starnation.android.util.Logger;
import com.starnation.util.StringUtil;

import java.util.Locale;

/*
 * @author lsh
 * @since 2016. 2. 25.
*/
public final class WidgetLogger {

    private final static String TITLE = "########################### %s ###########################";

    private static boolean sLoggable;

    public static void d(WidgetTag widgetTag, String message) {
        if (isLoggable(widgetTag)) {
            Logger.d(widgetTag.toTag(), message);
        }
    }

    public static void i(WidgetTag widgetTag, String message) {
        if (isLoggable(widgetTag)) {
            Logger.i(widgetTag.toTag(), message);
        }
    }

    public static void w(WidgetTag widgetTag, String message) {
        if (isLoggable(widgetTag)) {
            Logger.w(widgetTag.toTag(), message);
        }
    }

    public static void titleI(WidgetTag widgetTag, String message) {
        if (isLoggable(widgetTag)) {
            Logger.i(widgetTag.toTag(), String.format(Locale.US, TITLE, message));
        }
    }

    public static void e(WidgetTag widgetTag, String message) {
        if (isLoggable(widgetTag)) {
            Logger.e(widgetTag.toTag(), message);
        }
    }

    public static void titleE(WidgetTag widgetTag, String message) {
        if (isLoggable(widgetTag)) {
            Logger.e(widgetTag.toTag(), String.format(Locale.US, TITLE, message));
        }
    }

    public static void v(WidgetTag widgetTag, String message) {
        if (isLoggable(widgetTag)) {
            Logger.v(widgetTag.toTag(), message);
        }
    }

    public static boolean isLoggable() {
        return sLoggable;
    }

    public static void setLoggable(boolean loggable) {
        sLoggable = loggable;
    }

    public static void printStackTrace(Exception e) {
        if (sLoggable && e != null) {
            e.printStackTrace();
        }
    }

    public static String makeLogMessage(String tag, String message) {
        if (StringUtil.isEmpty(tag) == false) {
            return String.format(Locale.US, "[%s]: %s", tag, message);
        }
        return message;
    }

    //======================================================================
    // Private Methods
    //======================================================================

    private static boolean isLoggable(WidgetTag widgetTag) {
        return sLoggable == true && widgetTag.isEnable();
    }
}
